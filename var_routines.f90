! This module contains the routines that calculate the needed variables for JFNKsolver.f90
! Original Author: Clinton Seinen
!
! The contained routines are:
!       - calc_ice_strgth
!       - calc_ice_strgth_r02Test
!       - calc_visc_and_viscgrads
!       - calc_visc 
!       - calc_waterdrag_coef
!       - interpolate_h 
!       - CalcSave_Pstress
!       - CalcVol

module var_routines

! -----------------------------------------------------------------------------!
    !                       Module References                           !
    use modelparams         ! includes physical, numerical, and grid parameters
    use initialization      ! includes grid parameters that are defined according to dx and will remain unchanged
    use csv_file            ! module for writing data to csv format
    use domain_routines 
! -----------------------------------------------------------------------------!

implicit none

! -----------------------------------------------------------------------------!
    !                           Contains                                !
! -----------------------------------------------------------------------------!

contains
!===========================================================================================!
!                                   Calc Ice Strength                                       !
!===========================================================================================!
    subroutine calc_ice_strgth(P_T, P_N, P_u, P_v, h_T, A_T, dist_T, dist_gx_T, dist_gy_T, &   
                            indxUi, indxUj, indxVi, indxVj, indxTi, indxTj, indxNi, indxNj, &
                            nU_pnts, nV_pnts, nT_pnts, nN_pnts, nxU, nyU, nxV, nyV, nxT, nyT, nxN, nyN)

        !======================================================================================
        ! This subroutine calculates the ice strength at T points and smooths it near the ice
        ! pack terminus. Once calculated it then interpolates the smoothed strength field to 
        ! U, V, and N points.
        !
        ! NOTE: In order to avoid two ice packs interacting with each others smoothing process,
        !       this subroutine artifically assigns the ice strength and distance values outside
        !       the ice pack, as they should be if only open water was present. This is done
        !       because if two ice packs are near each other, the smoothing process for ice 
        !       strength may use unsmoothed strength and distance values from with-in
        !       the neighbouring ice pack when it should really be using values associated with
        !       open ocean.
        !
        ! NOTE: This routine populates a "strength halo", which is a halo of tracer points that 
        !       are close enough to the ice terminus such that some ice will be in the whole cell.
        !       This is done as P is supposed to reflect the average values across the cell, and 
        !       not just the strength at the T point. There may be a more efficient way to include 
        !       these points in the calculations, but what I have implemented (as of July 8th, 2017)
        !       was a quick implementation.
        !======================================================================================  

        !==================!
        !--------In--------!
        !==================!
        integer(kind = int_kind), intent(in) :: &
            nxU, nyU,                       &   ! indices limit for U points (not including ghost cells)
            nxV, nyV,                       &   ! indices limit for V points (not including ghost cells)
            nxT, nyT,                       &   ! indices limit for T points (not including ghost cells)
            nxN, nyN,                       &   ! indices limit for N points 
            nU_pnts, nV_pnts,               &   ! number of U and V points in the computational domain 
            nT_pnts, nN_pnts                    ! number of T and N points in the computational domain
        integer(kind = int_kind), dimension(nU_pnts), intent(in) :: &
            indxUi, indxUj                      ! locations of U points in the computational domain
        integer(kind = int_kind), dimension(nV_pnts), intent(in) :: &
            indxVi, indxVj                      ! locations of V points in the computational domain
        integer(kind = int_kind), dimension(nT_pnts), intent(in) :: &
            indxTi, indxTj                      ! locations of T points in the computational domain
        integer(kind = int_kind), dimension(nN_pnts) :: &
            indxNi, indxNj                      ! locations of N points in the comp. domain
        real(kind = dbl_kind), dimension(1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent(in) :: &
            h_T,    A_T,                    &   ! thickness and concentration fields
            dist_T, dist_gx_T,  dist_gy_T       ! distance function information
        
        !==================!    
        !------In/Out------!
        !==================!
        real(kind = dbl_kind), dimension(1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent(inout) :: &
            P_T                                 ! ice strength at T points
        real(kind = dbl_kind), dimension(nxN, nyN), intent(inout) :: &
            P_N                                 ! ice strength at N points
        real(kind = dbl_kind), dimension(nxU, nyU), intent(inout) :: &
            P_u                                 ! ice strength at U points 
        real(kind = dbl_kind), dimension(nxV, nyV), intent(inout) :: &
            P_v                                 ! ice strength at V points

        !==================!
        !----Local Vars----!
        !==================!
        integer(kind = int_kind) :: &
            i, j, ij, k, num, q,            &   ! local indices and counters
            count_inc, count_dec,           &
            ii, jj          
        real(kind = dbl_kind), dimension(1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T) :: &
            P_T_unsmoothed                      ! stores the pre-smoothed strength values
        real(kind = dbl_kind) :: &
            nrm_grd_x, nrm_grd_y,           &   ! normalized components of the gradient of the distance function
            mg_grd,                         &   ! magnitude of the gradient of the distance function
            numer, denom                        ! numerator and denominator for smoothing calculation
        real(kind = dbl_kind) :: &
            alpha,  &   ! angle used to divide gradient of distance function into 8 possible categories
            C, S,   &   ! used to classify direction of graidnet by comparing to abs(nrm_grd_y) and abs(nrm_grd_x)
            smthlim     ! used to define when we want artificial smoothing to be activated
        logical, dimension (1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T) :: &
            StrHalo     ! used the flag halo of tracers where a non-zero strength is needed !


            StrHalo = .False.
            
            smthlim = -dx/sqrt(2.)
            alpha   = pi/8_dbl_kind
            C       = cos(alpha)
            S       = sin(3*alpha)

            !=================================!
            ! Initialize Strength Arrays to 0 !
            !=================================!
                P_T = 0.d0
                P_N = 0.d0
                P_v = 0.d0
                P_u = 0.d0
                P_T_unsmoothed = 0.0d0

            !===========================================================!
            ! Calc Original P (non-smoothed) and locate halo of Tracers !
            !===========================================================!
                do ij = 1, nT_pnts
                    i = indxTi(ij)
                    j = indxTj(ij)

                    P_T_unsmoothed(i,j) = str_param*h_T(i,j)*exp(-conc_param*(1 - A_T(i,j)))

                    ! flag halo !
                    do ii = i-1, i+1
                        do jj = j-1, j+1
                            if ((dist_T(ii,jj) < 0) .and. (dist_T(ii,jj) > smthlim)) then 
                                StrHalo(ii,jj) = .True.
                            end if 
                        end do 
                    end do
                enddo

            !=======================================================!
            ! Calc P, smoothed and unsmoothed, in the Strength Halo !
            !=======================================================!
                ! Note: the unsmoothed value is only calc'd for these points 
                !       so they can used in the smooth calcs for points in ice 
                do i = 1-gc_T, nxT+gc_T 
                    do j = 1-gc_T, nyT+gc_T
                        if (StrHalo(i,j)) then 
                            P_T_unsmoothed(i,j) = str_param*h_T(i,j)*exp(-conc_param*(1 - A_T(i,j)))
                            P_T(i,j) = P_T_unsmoothed(i,j)
                        end if 
                    end do 
                end do 

            ! Store original P !
            ! open(unit=1, file = './output/UnsmoothedP.dat', status = 'unknown')
            ! call csv_write_dble_2d(1,P_T_unsmoothed)
            ! close(1)

            !======================================!
            ! Smooth inside ice, near the boundary !
            !======================================!
            do ij = 1, nT_pnts
                i = indxTi(ij)
                j = indxTj(ij)

                if ((dist_T(i,j) > 0) .and. (dist_T(i,j) < smth_num*dx)) then
                    ! the point is close enough to the ice terminus and is with-in the ice pack, thus the ice strength must be smoothed

                    ! normalize gradient !
                    mg_grd = sqrt(dist_gx_T(i,j)**2 + dist_gy_T(i,j)**2)
                    nrm_grd_x = dist_gx_T(i,j)/mg_grd
                    nrm_grd_y = dist_gy_T(i,j)/mg_grd

                    ! start with contribution from the cell that is undergoing smoothing !
                    numer = P_T_unsmoothed(i,j)*abs(tanh(dist_T(i,j)/dx)) 
                    denom = abs(tanh(dist_T(i,j)/dx))
                    
                    ! check direction !
                    if ((abs(nrm_grd_y) <= 1) .and. (abs(nrm_grd_y) >= S))then

                        !---------------------------------------------------------------------
                        ! the gradient is pointing, approximately, along the Y axis
                        !---------------------------------------------------------------------

                        count_inc = 0
                        count_dec = 0

                        ! increasing y direction !
                        do k = 1, smth_num

                            if ((dist_T(i,j+(k-1)) + dist_gy_T(i,j+(k-1))*dx) < smthlim) then
                                ! The (i,j+k) point SHOULD be outside the ice pack.
                                ! Use artificial distance values in updating the denominator
                                ! in the smoothing calculation, by extrapolating using the 
                                ! gradient of the distance function.

                                ! determine the number of needed, artificial, points
                                num = smth_num - count_inc

                                    do q = 1,num
                                        ! numerator not updated as P_T_unsmoothed should be zero
                                        denom = denom + abs(tanh(dist_T(i,j+(k-1))/dx + q*dist_gy_T(i,j+(k-1))))
                                    enddo

                                EXIT ! exit the outer loop

                            else
                                ! The (i,j+k) is with-in the ice pack.
                                ! Procede with normal loop.
                                numer = numer + P_T_unsmoothed(i,j+k)*abs(tanh(dist_T(i,j+k)/dx))
                                denom = denom + abs(tanh(dist_T(i,j+k)/dx))
                                count_inc = count_inc + 1
                            endif

                        enddo

                        ! decreasing y direction !
                        do k = 1, smth_num

                            if ((dist_T(i,j-(k-1)) - dist_gy_T(i,j-(k-1))*dx) < smthlim) then
                                ! The (i,j-k) point SHOULD be outside the ice pack.
                                ! Use artificial distance values in updating the denominator
                                ! in the smoothing calculation, by extrapolating using the 
                                ! gradient of the distance function.

                                ! determine the number of needed, artificial, points
                                num = smth_num - count_dec

                                    do q = 1,num
                                        ! numerator not updated as P_T_unsmoothed should be zero
                                        denom = denom + abs(tanh(dist_T(i,j-(k-1))/dx - q*dist_gy_T(i,j-(k-1))))
                                    enddo

                                EXIT ! exit the outer loop

                            else
                                ! The (i,j-k) is with-in the ice pack.
                                ! Procede with normal loop.
                                numer = numer + P_T_unsmoothed(i,j-k)*abs(tanh(dist_T(i,j-k)/dx))
                                denom = denom + abs(tanh(dist_T(i,j-k)/dx))
                                count_dec = count_dec + 1
                            endif

                        enddo     

                        ! Store Smoothed value
                        P_T(i,j) = numer/denom

                    elseif ((abs(nrm_grd_x) <= 1) .and. (abs(nrm_grd_x) >= C)) then

                        !---------------------------------------------------------------------
                        ! the gradient is pointing, approximately, along the x axis
                        !---------------------------------------------------------------------

                        count_inc = 0
                        count_dec = 0

                        ! increasing x direction !
                        do k = 1, smth_num

                            if ((dist_T(i+(k-1),j) + dist_gx_T(i+(k-1),j)*dx) < smthlim) then
                                ! The (i+k,j) point SHOULD be outside the ice pack.
                                ! Use artificial distance values in updating the denominator
                                ! in the smoothing calculation, by extrapolating using the 
                                ! gradient of the distance function.

                                ! determine the number of needed, artificial, points
                                num = smth_num - count_inc

                                    do q = 1,num
                                        ! numerator not updated as P_T_unsmoothed should be zero
                                        denom = denom + abs(tanh(dist_T(i+(k-1),j)/dx + q*dist_gx_T(i+(k-1),j)))
                                    enddo

                                EXIT ! exit the outer loop

                            else
                                ! The (i+k,j) is with-in the ice pack.
                                ! Procede with normal loop.
                                numer = numer + P_T_unsmoothed(i+k,j)*abs(tanh(dist_T(i+k,j)/dx))
                                denom = denom + abs(tanh(dist_T(i+k,j)/dx))
                                count_inc = count_inc + 1
                            endif

                        enddo

                        ! decreasing x direction !
                        do k = 1, smth_num
                            
                            if ((dist_T(i-(k-1),j) - dist_gx_T(i-(k-1),j)*dx) < smthlim) then
                                ! The (i-k,j) point SHOULD be outside the ice pack.
                                ! Use artificial distance values in updating the denominator
                                ! in the smoothing calculation, by extrapolating using the 
                                ! gradient of the distance function.

                                ! determine the number of needed, artificial, points
                                num = smth_num - count_dec

                                    do q = 1,num
                                        ! numerator not updated as P_T_unsmoothed should be zero
                                        denom = denom + abs(tanh(dist_T(i-(k-1),j)/dx - q*dist_gx_T(i-(k-1),j)))
                                    enddo

                                EXIT ! exit the outer loop

                            else
                                ! The (i-k,j) is with-in the ice pack.
                                ! Procede with normal loop.
                                numer = numer + P_T_unsmoothed(i-k,j)*abs(tanh(dist_T(i-k,j)/dx))
                                denom = denom + abs(tanh(dist_T(i-k,j)/dx))
                                count_dec = count_dec + 1
                            endif

                        enddo    

                        ! Store Smoothed value
                        P_T(i,j) = numer/denom

                    else 
                        if (nrm_grd_x*nrm_grd_y > 0) then

                            !---------------------------------------------------------------------
                            ! the gradient is pointing, approximately, along y = x
                            !---------------------------------------------------------------------

                            count_inc = 0
                            count_dec = 0

                            ! increasing x and y direction !
                            do k = 1, smth_num

                                if ((dist_T(i+(k-1),j+(k-1)) + dist_gx_T(i+(k-1),j+(k-1))*dx + dist_gy_T(i+(k-1),j+(k-1))*dx) < smthlim) then
                                    ! The (i+k,j+k) point SHOULD be outside the ice pack.
                                    ! Use artificial distance values in updating the denominator
                                    ! in the smoothing calculation, by extrapolating using the 
                                    ! gradient of the distance function.

                                    ! determine the number of needed, artificial, points
                                    num = smth_num - count_inc

                                        do q = 1,num
                                            ! numerator not updated as P_T_unsmoothed should be zero
                                            denom = denom + abs(tanh(dist_T(i+(k-1),j+(k-1))/dx + q*dist_gx_T(i+(k-1),j+(k-1)) + q*dist_gy_T(i+(k-1),j+(k-1))))
                                        enddo

                                    EXIT ! exit the outer loop

                                else
                                    ! The (i+k,j+k) point is with-in the ice pack.
                                    ! Procede with normal loop.
                                    numer = numer + P_T_unsmoothed(i+k,j+k)*abs(tanh(dist_T(i+k,j+k)/dx))
                                    denom = denom + abs(tanh(dist_T(i+k,j+k)/dx))
                                    count_inc = count_inc + 1
                                endif

                            enddo

                            ! decreasing x and y direction !
                            do k = 1, smth_num

                                if ((dist_T(i-(k-1),j-(k-1)) - dist_gx_T(i-(k-1),j-(k-1))*dx - dist_gy_T(i-(k-1),j-(k-1))*dx) < smthlim) then
                                    ! The (i-k,j-k) point SHOULD be outside the ice pack.
                                    ! Use artificial distance values in updating the denominator
                                    ! in the smoothing calculation, by extrapolating using the 
                                    ! gradient of the distance function.

                                    ! determine the number of needed, artificial, points
                                    num = smth_num - count_dec

                                        do q = 1,num
                                            ! numerator not updated as P_T_unsmoothed should be zero
                                            denom = denom + abs(tanh(dist_T(i-(k-1),j-(k-1))/dx - q*dist_gx_T(i-(k-1),j-(k-1)) - q*dist_gy_T(i-(k-1),j-(k-1))))
                                        enddo

                                    EXIT ! exit the outer loop

                                else
                                    ! The (i-k,j-k) point is with-in the ice pack.
                                    ! Procede with normal loop.
                                    numer = numer + P_T_unsmoothed(i-k,j-k)*abs(tanh(dist_T(i-k,j-k)/dx))
                                    denom = denom + abs(tanh(dist_T(i-k,j-k)/dx))
                                    count_dec = count_dec + 1
                                endif

                            enddo      

                            ! Store Smoothed value
                            P_T(i,j) = numer/denom

                        else

                            !---------------------------------------------------------------------
                            ! the gradient is pointing, approximately, along y = -x
                            !---------------------------------------------------------------------

                            count_inc = 0
                            count_dec = 0

                            ! increasing x direction !
                            ! decreasing y direction !
                            do k = 1, smth_num
                                if ((dist_T(i+(k-1),j-(k-1)) + dist_gx_T(i+(k-1),j-(k-1))*dx - dist_gy_T(i+(k-1),j-(k-1))*dx) < smthlim) then
                                    ! The (i+k,j-k) point SHOULD be outside the ice pack.
                                    ! Use artificial distance values in updating the denominator
                                    ! in the smoothing calculation, by extrapolating using the 
                                    ! gradient of the distance function.

                                    ! determine the number of needed, artificial, points
                                    num = smth_num - count_inc

                                        do q = 1,num
                                            ! numerator not updated as P_T_unsmoothed should be zero
                                            denom = denom + abs(tanh(dist_T(i+(k-1),j-(k-1))/dx + q*dist_gx_T(i+(k-1),j-(k-1)) - q*dist_gy_T(i+(k-1),j-(k-1))))
                                        enddo

                                    EXIT ! exit the outer loop

                                else
                                    ! The (i+k,j-k) point is with-in the ice pack.
                                    ! Procede with normal loop.
                                    numer = numer + P_T_unsmoothed(i+k,j-k)*abs(tanh(dist_T(i+k,j-k)/dx))
                                    denom = denom + abs(tanh(dist_T(i+k,j-k)/dx))
                                    count_inc = count_inc + 1
                                endif
                            enddo

                            ! decreasing x direction !
                            ! increasing y direction !
                            do k = 1, smth_num
                                if ((dist_T(i-(k-1),j+(k-1)) - dist_gx_T(i-(k-1),j+(k-1))*dx + dist_gy_T(i-(k-1),j+(k-1))*dx) < smthlim) then
                                    ! The (i-k,j+k) point SHOULD be outside the ice pack.
                                    ! Use artificial distance values in updating the denominator
                                    ! in the smoothing calculation, by extrapolating using the 
                                    ! gradient of the distance function.

                                    ! determine the number of needed, artificial, points
                                    num = smth_num - count_dec

                                        do q = 1,num
                                            ! numerator not updated as P_T_unsmoothed should be zero
                                            denom = denom + abs(tanh(dist_T(i-(k-1),j+(k-1))/dx - q*dist_gx_T(i-(k-1),j+(k-1)) + q*dist_gy_T(i-(k-1),j+(k-1))))
                                        enddo

                                    EXIT ! exit the outer loop

                                else
                                    ! The (i-k,j+k) point is with-in the ice pack.
                                    ! Procede with normal loop.
                                    numer = numer + P_T_unsmoothed(i-k,j+k)*abs(tanh(dist_T(i-k,j+k)/dx))
                                    denom = denom + abs(tanh(dist_T(i-k,j+k)/dx))
                                    count_dec = count_dec + 1
                                endif
                            enddo      

                            ! Store Smoothed value
                            P_T(i,j) = numer/denom    

                        endif
                    endif
                else
                    ! the point is far enough from the ice front, or is outside the ice pack; smoothing for P is not required
                    P_T(i,j) = P_T_unsmoothed(i,j)
                endif
            enddo 

            !=======================================================!
            ! Store Smoothed Values and area fraction and thickness !
            !=======================================================!
                ! open(unit = 1, file = './output/smoothedP.dat', status = 'unknown')
                ! call csv_write_dble_2d(1, P_T)
                ! close(1)

                ! open(unit = 1, file = './output/h.dat', status = 'unknown')
                ! open(unit = 2, file = './output/A.dat', status = 'unknown')
                ! open(unit = 3, file = './output/phi.dat', status = 'unknown')
                ! call csv_write_dble_2d(1, h_T)
                ! call csv_write_dble_2d(2, A_T)
                ! call csv_write_dble_2d(3, dist_T)
                ! close(1)
                ! close(2)
                ! close(3)

            !==== Interpolate Ice Strength to N, U, and V points  =====!
            do ij = 1, nN_pnts
                i = indxNi(ij)
                j = indxNj(ij)

                P_N(i,j) = (P_T(i,j) + P_T(i-1,j) + P_T(i-1,j-1) + P_T(i,j-1))/4
            enddo
            do ij = 1, nU_pnts
                i = indxUi(ij)
                j = indxUj(ij)

                P_u(i,j) = (P_T(i,j) + P_T(i-1,j))/2
            enddo
            do ij = 1, nV_pnts
                i = indxVi(ij)
                j = indxVj(ij)

                P_v(i,j) = (P_T(i,j) + P_T(i,j-1))/2
            enddo
    end subroutine calc_ice_strgth

!===========================================================================================!
!                                   Calc Ice Strength - rev2 Test                           !
!===========================================================================================!
    subroutine calc_ice_strgth_r02Test(P_T, P_N, P_u, P_v, h_T, A_T, dist_T, dist_gx_T, dist_gy_T, &   
                            indxUi, indxUj, indxVi, indxVj, indxTi, indxTj, indxNi, indxNj, &
                            nU_pnts, nV_pnts, nT_pnts, nN_pnts, nxU, nyU, nxV, nyV, nxT, nyT, nxN, nyN)

        !======================================================================================
        ! This routine was made as a test, as it was noted that by allowing calculations of ice 
        ! strength justoutside the ice pack, this has lead to some interference caused by 
        ! neighbouring ice packs. It shouldn't have much effect, but this routine was created
        ! to test if a significant difference would be made if a version of it was made that 
        ! tried a different method.
        !======================================================================================  

        !==================!
        !--------In--------!
        !==================!
        integer(kind = int_kind), intent(in) :: &
            nxU, nyU,                       &   ! indices limit for U points (not including ghost cells)
            nxV, nyV,                       &   ! indices limit for V points (not including ghost cells)
            nxT, nyT,                       &   ! indices limit for T points (not including ghost cells)
            nxN, nyN,                       &   ! indices limit for N points 
            nU_pnts, nV_pnts,               &   ! number of U and V points in the computational domain 
            nT_pnts, nN_pnts                    ! number of T and N points in the computational domain
        integer(kind = int_kind), dimension(nU_pnts), intent(in) :: &
            indxUi, indxUj                      ! locations of U points in the computational domain
        integer(kind = int_kind), dimension(nV_pnts), intent(in) :: &
            indxVi, indxVj                      ! locations of V points in the computational domain
        integer(kind = int_kind), dimension(nT_pnts), intent(in) :: &
            indxTi, indxTj                      ! locations of T points in the computational domain
        integer(kind = int_kind), dimension(nN_pnts) :: &
            indxNi, indxNj                      ! locations of N points in the comp. domain
        real(kind = dbl_kind), dimension(1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent(in) :: &
            h_T,    A_T,                    &   ! thickness and concentration fields
            dist_T, dist_gx_T,  dist_gy_T       ! distance function information
        
        !==================!    
        !------In/Out------!
        !==================!
        real(kind = dbl_kind), dimension(1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent(inout) :: &
            P_T                                 ! ice strength at T points
        real(kind = dbl_kind), dimension(nxN, nyN), intent(inout) :: &
            P_N                                 ! ice strength at N points
        real(kind = dbl_kind), dimension(nxU, nyU), intent(inout) :: &
            P_u                                 ! ice strength at U points 
        real(kind = dbl_kind), dimension(nxV, nyV), intent(inout) :: &
            P_v                                 ! ice strength at V points

        !==================!
        !----Local Vars----!
        !==================!
        integer(kind = int_kind) :: &
            i, j, ij, k, num, q,            &   ! local indices and counters
            count_inc, count_dec,           &
            ii, jj          
        real(kind = dbl_kind), dimension(1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T) :: &
            P_T_unsmoothed                      ! stores the pre-smoothed strength values
        real(kind = dbl_kind) :: &
            nrm_grd_x, nrm_grd_y,           &   ! normalized components of the gradient of the distance function
            mg_grd,                         &   ! magnitude of the gradient of the distance function
            numer, denom                        ! numerator and denominator for smoothing calculation
        real(kind = dbl_kind) :: &
            alpha,  &   ! angle used to divide gradient of distance function into 8 possible categories
            C, S,   &   ! used to classify direction of graidnet by comparing to abs(nrm_grd_y) and abs(nrm_grd_x)
            smthlim     ! used to define when we want artificial smoothing to be activated
        logical, dimension (1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T) :: &
            StrHalo     ! used the flag halo of tracers where a non-zero strength is needed !


            StrHalo = .False.
            
            smthlim = -dx/sqrt(2.)
            alpha   = pi/8_dbl_kind
            C       = cos(alpha)
            S       = sin(3*alpha)

            !=================================!
            ! Initialize Strength Arrays to 0 !
            !=================================!
                P_T = 0.d0
                P_N = 0.d0
                P_v = 0.d0
                P_u = 0.d0
                P_T_unsmoothed = 0.0d0

            !===========================================================!
            ! Calc Original P (non-smoothed) and locate halo of Tracers !
            !===========================================================!
                do ij = 1, nT_pnts
                    i = indxTi(ij)
                    j = indxTj(ij)

                    P_T_unsmoothed(i,j) = str_param*h_T(i,j)*exp(-conc_param*(1 - A_T(i,j)))

                    ! flag halo !
                    do ii = i-1, i+1
                        do jj = j-1, j+1
                            if ((dist_T(ii,jj) < 0) .and. (dist_T(ii,jj) > smthlim)) then 
                                StrHalo(ii,jj) = .True.
                            end if 
                        end do 
                    end do
                enddo

            !=======================================================!
            ! Calc P, smoothed and unsmoothed, in the Strength Halo !
            !=======================================================!
                ! Note: the unsmoothed value is only calc'd for these points 
                !       so they can used in the smooth calcs for points in ice 
                do i = 1-gc_T, nxT+gc_T 
                    do j = 1-gc_T, nyT+gc_T
                        if (StrHalo(i,j)) then 
                            P_T_unsmoothed(i,j) = str_param*h_T(i,j)*exp(-conc_param*(1 - A_T(i,j)))
                            P_T(i,j) = P_T_unsmoothed(i,j)
                        end if 
                    end do 
                end do 

            ! Store original P !
            ! open(unit=1, file = './output/UnsmoothedP.dat', status = 'unknown')
            ! call csv_write_dble_2d(1,P_T_unsmoothed)
            ! close(1)

            !======================================!
            ! Smooth inside ice, near the boundary !
            !======================================!
            do ij = 1, nT_pnts
                i = indxTi(ij)
                j = indxTj(ij)

                if ((dist_T(i,j) > 0) .and. (dist_T(i,j) < smth_num*dx)) then
                    ! the point is close enough to the ice terminus and is with-in the ice pack, thus the ice strength must be smoothed

                    ! normalize gradient !
                    mg_grd = sqrt(dist_gx_T(i,j)**2 + dist_gy_T(i,j)**2)
                    nrm_grd_x = dist_gx_T(i,j)/mg_grd
                    nrm_grd_y = dist_gy_T(i,j)/mg_grd

                    ! start with contribution from the cell that is undergoing smoothing !
                    numer = P_T_unsmoothed(i,j)*abs(tanh(dist_T(i,j)/dx)) 
                    denom = abs(tanh(dist_T(i,j)/dx))
                    
                    ! check direction !
                    if ((abs(nrm_grd_y) <= 1) .and. (abs(nrm_grd_y) >= S))then

                        !---------------------------------------------------------------------
                        ! the gradient is pointing, approximately, along the Y axis
                        !---------------------------------------------------------------------

                        count_inc = 0
                        count_dec = 0

                        ! increasing y direction !
                        do k = 1, smth_num

                            if (((dist_T(i,j+(k-1)) + dist_gy_T(i,j+(k-1))*dx) < 0.d0) .and. (.not. StrHalo(i,j+k))) then
                                ! The (i,j+k) point SHOULD be outside the ice pack and isn't a str halo.
                                ! Use artificial distance values in updating the denominator
                                ! in the smoothing calculation, by extrapolating using the 
                                ! gradient of the distance function.

                                ! determine the number of needed, artificial, points
                                num = smth_num - count_inc

                                    do q = 1,num
                                        ! numerator not updated as P_T_unsmoothed should be zero
                                        denom = denom + abs(tanh(dist_T(i,j+(k-1))/dx + q*dist_gy_T(i,j+(k-1))))
                                    enddo

                                EXIT ! exit the outer loop

                            else
                                ! The (i,j+k) is with-in the ice pack.
                                ! Procede with normal loop.
                                numer = numer + P_T_unsmoothed(i,j+k)*abs(tanh(dist_T(i,j+k)/dx))
                                denom = denom + abs(tanh(dist_T(i,j+k)/dx))
                                count_inc = count_inc + 1
                            endif

                        enddo

                        ! decreasing y direction !
                        do k = 1, smth_num

                            if (((dist_T(i,j-(k-1)) - dist_gy_T(i,j-(k-1))*dx) < 0.d0) .and. (.not. StrHalo(i,j-k))) then
                                ! The (i,j-k) point SHOULD be outside the ice pack.
                                ! Use artificial distance values in updating the denominator
                                ! in the smoothing calculation, by extrapolating using the 
                                ! gradient of the distance function.

                                ! determine the number of needed, artificial, points
                                num = smth_num - count_dec

                                    do q = 1,num
                                        ! numerator not updated as P_T_unsmoothed should be zero
                                        denom = denom + abs(tanh(dist_T(i,j-(k-1))/dx - q*dist_gy_T(i,j-(k-1))))
                                    enddo

                                EXIT ! exit the outer loop

                            else
                                ! The (i,j-k) is with-in the ice pack.
                                ! Procede with normal loop.
                                numer = numer + P_T_unsmoothed(i,j-k)*abs(tanh(dist_T(i,j-k)/dx))
                                denom = denom + abs(tanh(dist_T(i,j-k)/dx))
                                count_dec = count_dec + 1
                            endif

                        enddo     

                        ! Store Smoothed value
                        P_T(i,j) = numer/denom

                    elseif ((abs(nrm_grd_x) <= 1) .and. (abs(nrm_grd_x) >= C)) then

                        !---------------------------------------------------------------------
                        ! the gradient is pointing, approximately, along the x axis
                        !---------------------------------------------------------------------

                        count_inc = 0
                        count_dec = 0

                        ! increasing x direction !
                        do k = 1, smth_num

                            if (((dist_T(i+(k-1),j) + dist_gx_T(i+(k-1),j)*dx) < 0.d0) .and. (.not. StrHalo(i+k,j))) then
                                ! The (i+k,j) point SHOULD be outside the ice pack.
                                ! Use artificial distance values in updating the denominator
                                ! in the smoothing calculation, by extrapolating using the 
                                ! gradient of the distance function.

                                ! determine the number of needed, artificial, points
                                num = smth_num - count_inc

                                    do q = 1,num
                                        ! numerator not updated as P_T_unsmoothed should be zero
                                        denom = denom + abs(tanh(dist_T(i+(k-1),j)/dx + q*dist_gx_T(i+(k-1),j)))
                                    enddo

                                EXIT ! exit the outer loop

                            else
                                ! The (i+k,j) is with-in the ice pack.
                                ! Procede with normal loop.
                                numer = numer + P_T_unsmoothed(i+k,j)*abs(tanh(dist_T(i+k,j)/dx))
                                denom = denom + abs(tanh(dist_T(i+k,j)/dx))
                                count_inc = count_inc + 1
                            endif

                        enddo

                        ! decreasing x direction !
                        do k = 1, smth_num
                            
                            if (((dist_T(i-(k-1),j) - dist_gx_T(i-(k-1),j)*dx) < 0.d0) .and. (.not. StrHalo(i-k,j))) then
                                ! The (i-k,j) point SHOULD be outside the ice pack.
                                ! Use artificial distance values in updating the denominator
                                ! in the smoothing calculation, by extrapolating using the 
                                ! gradient of the distance function.

                                ! determine the number of needed, artificial, points
                                num = smth_num - count_dec

                                    do q = 1,num
                                        ! numerator not updated as P_T_unsmoothed should be zero
                                        denom = denom + abs(tanh(dist_T(i-(k-1),j)/dx - q*dist_gx_T(i-(k-1),j)))
                                    enddo

                                EXIT ! exit the outer loop

                            else
                                ! The (i-k,j) is with-in the ice pack.
                                ! Procede with normal loop.
                                numer = numer + P_T_unsmoothed(i-k,j)*abs(tanh(dist_T(i-k,j)/dx))
                                denom = denom + abs(tanh(dist_T(i-k,j)/dx))
                                count_dec = count_dec + 1
                            endif

                        enddo    

                        ! Store Smoothed value
                        P_T(i,j) = numer/denom

                    else 
                        if (nrm_grd_x*nrm_grd_y > 0) then

                            !---------------------------------------------------------------------
                            ! the gradient is pointing, approximately, along y = x
                            !---------------------------------------------------------------------

                            count_inc = 0
                            count_dec = 0

                            ! increasing x and y direction !
                            do k = 1, smth_num

                                if (((dist_T(i+(k-1),j+(k-1)) + dist_gx_T(i+(k-1),j+(k-1))*dx + dist_gy_T(i+(k-1),j+(k-1))*dx) < 0.d0) .and. (.not. StrHalo(i+k,j+k))) then
                                    ! The (i+k,j+k) point SHOULD be outside the ice pack.
                                    ! Use artificial distance values in updating the denominator
                                    ! in the smoothing calculation, by extrapolating using the 
                                    ! gradient of the distance function.

                                    ! determine the number of needed, artificial, points
                                    num = smth_num - count_inc

                                        do q = 1,num
                                            ! numerator not updated as P_T_unsmoothed should be zero
                                            denom = denom + abs(tanh(dist_T(i+(k-1),j+(k-1))/dx + q*dist_gx_T(i+(k-1),j+(k-1)) + q*dist_gy_T(i+(k-1),j+(k-1))))
                                        enddo

                                    EXIT ! exit the outer loop

                                else
                                    ! The (i+k,j+k) point is with-in the ice pack.
                                    ! Procede with normal loop.
                                    numer = numer + P_T_unsmoothed(i+k,j+k)*abs(tanh(dist_T(i+k,j+k)/dx))
                                    denom = denom + abs(tanh(dist_T(i+k,j+k)/dx))
                                    count_inc = count_inc + 1
                                endif

                            enddo

                            ! decreasing x and y direction !
                            do k = 1, smth_num

                                if (((dist_T(i-(k-1),j-(k-1)) - dist_gx_T(i-(k-1),j-(k-1))*dx - dist_gy_T(i-(k-1),j-(k-1))*dx) < 0.d0) .and. (.not. StrHalo(i-k,j-k))) then
                                    ! The (i-k,j-k) point SHOULD be outside the ice pack.
                                    ! Use artificial distance values in updating the denominator
                                    ! in the smoothing calculation, by extrapolating using the 
                                    ! gradient of the distance function.

                                    ! determine the number of needed, artificial, points
                                    num = smth_num - count_dec

                                        do q = 1,num
                                            ! numerator not updated as P_T_unsmoothed should be zero
                                            denom = denom + abs(tanh(dist_T(i-(k-1),j-(k-1))/dx - q*dist_gx_T(i-(k-1),j-(k-1)) - q*dist_gy_T(i-(k-1),j-(k-1))))
                                        enddo

                                    EXIT ! exit the outer loop

                                else
                                    ! The (i-k,j-k) point is with-in the ice pack.
                                    ! Procede with normal loop.
                                    numer = numer + P_T_unsmoothed(i-k,j-k)*abs(tanh(dist_T(i-k,j-k)/dx))
                                    denom = denom + abs(tanh(dist_T(i-k,j-k)/dx))
                                    count_dec = count_dec + 1
                                endif

                            enddo      

                            ! Store Smoothed value
                            P_T(i,j) = numer/denom

                        else

                            !---------------------------------------------------------------------
                            ! the gradient is pointing, approximately, along y = -x
                            !---------------------------------------------------------------------

                            count_inc = 0
                            count_dec = 0

                            ! increasing x direction !
                            ! decreasing y direction !
                            do k = 1, smth_num
                                if (((dist_T(i+(k-1),j-(k-1)) + dist_gx_T(i+(k-1),j-(k-1))*dx - dist_gy_T(i+(k-1),j-(k-1))*dx) < 0.d0) .and. (.not. StrHalo(i+k,j-k))) then
                                    ! The (i+k,j-k) point SHOULD be outside the ice pack.
                                    ! Use artificial distance values in updating the denominator
                                    ! in the smoothing calculation, by extrapolating using the 
                                    ! gradient of the distance function.

                                    ! determine the number of needed, artificial, points
                                    num = smth_num - count_inc

                                        do q = 1,num
                                            ! numerator not updated as P_T_unsmoothed should be zero
                                            denom = denom + abs(tanh(dist_T(i+(k-1),j-(k-1))/dx + q*dist_gx_T(i+(k-1),j-(k-1)) - q*dist_gy_T(i+(k-1),j-(k-1))))
                                        enddo

                                    EXIT ! exit the outer loop

                                else
                                    ! The (i+k,j-k) point is with-in the ice pack.
                                    ! Procede with normal loop.
                                    numer = numer + P_T_unsmoothed(i+k,j-k)*abs(tanh(dist_T(i+k,j-k)/dx))
                                    denom = denom + abs(tanh(dist_T(i+k,j-k)/dx))
                                    count_inc = count_inc + 1
                                endif
                            enddo

                            ! decreasing x direction !
                            ! increasing y direction !
                            do k = 1, smth_num
                                if (((dist_T(i-(k-1),j+(k-1)) - dist_gx_T(i-(k-1),j+(k-1))*dx + dist_gy_T(i-(k-1),j+(k-1))*dx) < 0.d0) .and. (.not. StrHalo(i-k,j+k))) then
                                    ! The (i-k,j+k) point SHOULD be outside the ice pack.
                                    ! Use artificial distance values in updating the denominator
                                    ! in the smoothing calculation, by extrapolating using the 
                                    ! gradient of the distance function.

                                    ! determine the number of needed, artificial, points
                                    num = smth_num - count_dec

                                        do q = 1,num
                                            ! numerator not updated as P_T_unsmoothed should be zero
                                            denom = denom + abs(tanh(dist_T(i-(k-1),j+(k-1))/dx - q*dist_gx_T(i-(k-1),j+(k-1)) + q*dist_gy_T(i-(k-1),j+(k-1))))
                                        enddo

                                    EXIT ! exit the outer loop

                                else
                                    ! The (i-k,j+k) point is with-in the ice pack.
                                    ! Procede with normal loop.
                                    numer = numer + P_T_unsmoothed(i-k,j+k)*abs(tanh(dist_T(i-k,j+k)/dx))
                                    denom = denom + abs(tanh(dist_T(i-k,j+k)/dx))
                                    count_dec = count_dec + 1
                                endif
                            enddo      

                            ! Store Smoothed value
                            P_T(i,j) = numer/denom    

                        endif
                    endif
                else
                    ! the point is far enough from the ice front, or is outside the ice pack; smoothing for P is not required
                    P_T(i,j) = P_T_unsmoothed(i,j)
                endif
            enddo 

            !=======================================================!
            ! Store Smoothed Values and area fraction and thickness !
            !=======================================================!
                ! open(unit = 1, file = './output/smoothedP.dat', status = 'unknown')
                ! call csv_write_dble_2d(1, P_T)
                ! close(1)

                ! open(unit = 1, file = './output/h.dat', status = 'unknown')
                ! open(unit = 2, file = './output/A.dat', status = 'unknown')
                ! open(unit = 3, file = './output/phi.dat', status = 'unknown')
                ! call csv_write_dble_2d(1, h_T)
                ! call csv_write_dble_2d(2, A_T)
                ! call csv_write_dble_2d(3, dist_T)
                ! close(1)
                ! close(2)
                ! close(3)

            !==== Interpolate Ice Strength to N, U, and V points  =====!
            do ij = 1, nN_pnts
                i = indxNi(ij)
                j = indxNj(ij)

                P_N(i,j) = (P_T(i,j) + P_T(i-1,j) + P_T(i-1,j-1) + P_T(i,j-1))/4
            enddo
            do ij = 1, nU_pnts
                i = indxUi(ij)
                j = indxUj(ij)

                P_u(i,j) = (P_T(i,j) + P_T(i-1,j))/2
            enddo
            do ij = 1, nV_pnts
                i = indxVi(ij)
                j = indxVj(ij)

                P_v(i,j) = (P_T(i,j) + P_T(i,j-1))/2
            enddo
    end subroutine calc_ice_strgth_r02Test

!===========================================================================================!
!                             Calculates Viscosities and Visc Grads                         !
!===========================================================================================!
    subroutine calc_visc_and_viscgrads(zeta_u, zeta_v, zeta_gx_u, zeta_gy_v, eta_u, eta_v, &
                                        eta_gx_u, eta_gx_v, eta_gy_u, eta_gy_v, P_u, P_v, P_T, P_N, ugrid, vgrid, time, &
                                        vlmsk, vimsk, ulmsk, uimsk, indxUi, indxUj, indxVi, indxVj, &
                                        nU_pnts, nV_pnts, nxU, nyU, nxV, nyV, nxT, nyT, nxN, nyN)

        !=======================================================================================
        ! This subroutine calculates the bulk and shear viscosities and their necessary gradients 
        ! at u and v points. 
        !
        ! Note that this routine replaced the original calc_visc routine, which calculated 
        ! viscosities at T and N points according to a non-continuously differentiable 
        ! method. This routine now uses the continuously differentiable formulation described
        ! in Lemiuex's paper.
        !=======================================================================================

        !==================!
        !------- In--------!
        !==================!
        integer(kind = int_kind), intent(in) :: &
            nxU, nyU, nxV, nyV,     &   ! index limits for U and V points 
            nxT, nyT, nxN, nyN,     &   ! index limits for T and N points 
            nU_pnts, nV_pnts            ! number of U and V points in the comp domain
        integer(kind = int_kind), dimension(nU_pnts), intent(in) :: &
            indxUi, indxUj              ! location of U points in the comp domain 
        integer(kind = int_kind), dimension(nV_pnts), intent(in) :: &
            indxVi, indxVj              ! location of V points in the comp domain 
        integer(kind = int_kind), dimension(nxU, nyU, 20), intent(in) :: &
            ulmsk, uimsk                ! computational mask used to set BC's 
        integer(kind = int_kind), dimension(nxV, nyV, 20), intent(in) :: &
            vlmsk, vimsk                ! computational mask used to set BC's 

        real(kind = dbl_kind), intent(in) :: &
            time                        ! current time level 
        real(kind = dbl_kind), dimension(1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U), intent(in) :: &
            ugrid                       ! u velocities 
        real(kind = dbl_kind), dimension(1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V), intent(in) :: &
            vgrid                       ! v velocities 
        real(kind = dbl_kind), dimension(nxU, nyU), intent(in) :: &
            P_u                         ! ice strength at U points
        real(kind = dbl_kind), dimension(nxV, nyV), intent(in) :: &
            P_v                         ! ice strength at V points 
        real(kind = dbl_kind), dimension(1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent(in) :: &
            P_T                         ! ice strength at T points 
        real(kind = dbl_kind), dimension(nxN, nyN), intent(in) :: &
            P_N                         ! ice strength at N points 

        !==================!
        !----- In/out -----!
        !==================!
        real(kind = dbl_kind), dimension(nxU, nyU), intent(inout) :: &
            zeta_u, eta_u,                  &   ! viscosities at U points 
            zeta_gx_u, eta_gx_u, eta_gy_u       ! needed visc gradients at U points  
        real(kind = dbl_kind), dimension(nxV, nyV), intent(inout) :: &
            zeta_v, eta_v,                  &   ! viscosities at V points 
            zeta_gy_v, eta_gx_v, eta_gy_v       ! needed visc gradients at V points 

        !==================!
        !--- Local Vars ---!
        !==================!
        character(len=8) :: &
            fmt, dx_f      ! local format descriptor for outputting files and a string to hold dx in kilometers for easy outputting
        integer(kind = int_kind) :: &
            i, j, ij 
        real(kind = dbl_kind) :: & 
            u_p0m1, u_m1p0, u_p1p0, u_p0p1, u_p1m1,         &   ! masked velocities
            u_p0p0, u_m1m1, u_m1p1, u_p1p1,                 &   
            u_p0m2, u_p1m2, u_p2m1, u_p2p0,                 &
            v_m1p0, v_p0p0, v_m1p1, v_p0p1, v_p0m1,         &
            v_p1p0, v_m1m1, v_p1m1, v_p1p1,                 &
            v_m2p0, v_m2p1, v_m1p2, v_p0p2,                 &   
            dudx, dudy, d2udx2, d2udy2, d2udxdy,            &   ! needed spatial derivatives in u 
            dvdx, dvdy, d2vdx2, d2vdy2, d2vdxdy,            &   ! needed spatial derivatives in v
            strain_11, strain_22, strain_12,                &   ! needed strains 
            delta, ddelta2_dx, ddelta2_dy,                  &   ! delta needed in calculations and the derivatives of its square
            dPdx, dPdy,                                     &   ! necessary ice strength derivatives
            halfdx, cnst1, cnst2, cnst3, cnst4                  ! constants to clean up code
        ! real(kind = dbl_kind), dimension (nxU, nyU) :: &
        !     deriv_U        ! arrays implemented to test convergence rates
        ! real(kind = dbl_kind), dimension (nxV, nyV) :: & 
        !     deriv_V        ! arrays implemented to test convergence rates

            fmt = '(I2.2)'
            write (dx_f, fmt) int(dx/1000) ! turn dx into a string 

            ! deriv_U = 0.; deriv_V = 0.

            halfdx  = dx/2
            cnst1   = 1./(2*dx)
            cnst2   = 1./(dx*dx)

            !=======================!
            !       U points        !
            !=======================!
            do ij = 1, nU_pnts
                i = indxUi(ij)
                j = indxUj(ij)

                ! Check for Masking !
                if ((product(ulmsk(i,j,:)) == 0) .or. (product(uimsk(i,j,:)) == 0)) then 

                    !===========================!
                    ! mask velocities as needed !
                    !===========================!   

                        !=======================!
                        !  Direct U Neighbours  !
                        !=======================!
                            u_p0m1 = ulmsk(i,j,1)*(uimsk(i,j,1)*ugrid(i  ,j-1)  +   (1 - uimsk(i,j,1))*(ugrid(i  ,j  ) - dx*dudyBC(x_u(i), y_u(j) - halfdx, time))) + (1 - ulmsk(i,j,1))*uBC(x_u(i)  , y_u(j-1), time)
                            u_m1p0 = ulmsk(i,j,2)*(uimsk(i,j,2)*ugrid(i-1,j  )  +   (1 - uimsk(i,j,2))*(ugrid(i  ,j  ) - dx*dudxBC(x_u(i) - halfdx, y_u(j), time))) + (1 - ulmsk(i,j,2))*uBC(x_u(i-1), y_u(j  ), time)
                            u_p1p0 = ulmsk(i,j,3)*(uimsk(i,j,3)*ugrid(i+1,j  )  +   (1 - uimsk(i,j,3))*(ugrid(i  ,j  ) + dx*dudxBC(x_u(i) + halfdx, y_u(j), time))) + (1 - ulmsk(i,j,3))*uBC(x_u(i+1), y_u(j  ), time)
                            u_p0p1 = ulmsk(i,j,4)*(uimsk(i,j,4)*ugrid(i  ,j+1)  +   (1 - uimsk(i,j,4))*(ugrid(i  ,j  ) + dx*dudyBC(x_u(i), y_u(j) + halfdx, time))) + (1 - ulmsk(i,j,4))*uBC(x_u(i  ), y_u(j+1), time)

                        !=======================!
                        !  Direct V Neighbours  !
                        !=======================!

                            ! v(i-1,j) !
                            v_m1p0 = ulmsk(i,j,5)*(uimsk(i,j,5)*                                     vgrid(i-1,j  ) &
                                    +   uimsk(i,j,6)*(1 - uimsk(i,j,5))*                            (vgrid(i  ,j  ) - dx*dvdxBC(x_v(i) - halfdx, y_v(j), time)) &
                                    +   uimsk(i,j,7)*(1 - uimsk(i,j,6))*(1 - uimsk(i,j,5))*         (vgrid(i-1,j+1) - dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time)) &
                                    +   (1 - uimsk(i,j,7))*(1 - uimsk(i,j,6))*(1 - uimsk(i,j,5))*   (vgrid(i  ,j+1) - dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time) - dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time))) &
                                    +   (1 - ulmsk(i,j,5))*vBC(x_v(i-1), y_v(j), time)

                            ! v(i,j) !
                            v_p0p0 = ulmsk(i,j,6)*(uimsk(i,j,6)*                                     vgrid(i  ,j  ) &  
                                    +   uimsk(i,j,5)*(1 - uimsk(i,j,6))*                            (vgrid(i-1,j  ) + dx*dvdxBC(x_v(i) - halfdx, y_v(j), time)) &  
                                    +   uimsk(i,j,8)*(1 - uimsk(i,j,5))*(1 - uimsk(i,j,6))*         (vgrid(i  ,j+1) - dx*dvdyBC(x_v(i), y_v(j) + halfdx, time)) &
                                    +   (1 - uimsk(i,j,8))*(1 - uimsk(i,j,5))*(1 - uimsk(i,j,6))*   (vgrid(i-1,j+1) + dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time) - dx*dvdyBC(x_v(i), y_v(j) + halfdx, time))) &
                                    +   (1 - ulmsk(i,j,6))*vBC(x_v(i), y_v(j), time)

                            ! v(i-1,j+1) !
                            v_m1p1 = ulmsk(i,j,7)*(uimsk(i,j,7)*                                     vgrid(i-1,j+1) &  
                                    +   uimsk(i,j,8)*(1 - uimsk(i,j,7))*                            (vgrid(i  ,j+1) - dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time)) &  
                                    +   uimsk(i,j,5)*(1 - uimsk(i,j,8))*(1 - uimsk(i,j,7))*         (vgrid(i-1,j  ) + dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time)) & 
                                    +   (1 - uimsk(i,j,5))*(1 - uimsk(i,j,8))*(1 - uimsk(i,j,7))*   (vgrid(i  ,j  ) - dx*dvdxBC(x_v(i) - halfdx, y_v(j), time) + dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time))) &
                                    +   (1 - ulmsk(i,j,7))*vBC(x_v(i-1), y_v(j+1), time)

                            ! v(i,j+1) !
                            v_p0p1 = ulmsk(i,j,8)*(uimsk(i,j,8)*                                     vgrid(i  ,j+1) &
                                    +   uimsk(i,j,7)*(1 - uimsk(i,j,8))*                            (vgrid(i-1,j+1) + dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time)) &  
                                    +   uimsk(i,j,6)*(1 - uimsk(i,j,7))*(1 - uimsk(i,j,8))*         (vgrid(i  ,j  ) + dx*dvdyBC(x_v(i), y_v(j) + halfdx, time )) & 
                                    +   (1 - uimsk(i,j,6))*(1 - uimsk(i,j,7))*(1 - uimsk(i,j,8))*   (vgrid(i-1,j  ) + dx*dvdxBC(x_v(i) - halfdx, y_v(j), time) + dx*dvdyBC(x_v(i), y_v(j) + halfdx, time))) &
                                    +   (1 - ulmsk(i,j,8))*vBC(x_v(i), y_v(j+1), time)

                        !============================================!
                        ! U points needed for mixed derivatives in U !
                        !============================================!

                            ! u(i-1,j-1) !
                            u_m1m1 = ulmsk(i,j,9)*(uimsk(i,j,9)*                                     ugrid(i-1,j-1) &
                                    + uimsk(i,j,2)*(1 - uimsk(i,j,9))*                              (ugrid(i-1,j  ) - dx*dudyBC(x_u(i-1), y_u(j-1) + halfdx, time)) & 
                                    + uimsk(i,j,1)*(1 - uimsk(i,j,2))*(1 - uimsk(i,j,9))*           (ugrid(i  ,j-1) - dx*dudxBC(x_u(i-1) + halfdx, y_u(j-1), time)) &
                                    + (1 - uimsk(i,j,1))*(1 - uimsk(i,j,2))*(1 - uimsk(i,j,9))*     (ugrid(i  ,j  ) - dx*dudxBC(x_u(i-1) + halfdx, y_u(j  ), time) - dx*dudyBC(x_u(i-1), y_u(j-1) + halfdx, time))) &
                                    + (1 - ulmsk(i,j,9))*uBC(x_u(i-1), y_u(j-1), time)

                            ! u(i+1,j-1) !
                            u_p1m1 = ulmsk(i,j,10)*(uimsk(i,j,10)*                                   ugrid(i+1,j-1) &
                                    + uimsk(i,j,3)*(1 - uimsk(i,j,10))*                             (ugrid(i+1,j  ) - dx*dudyBC(x_u(i+1), y_u(j-1) + halfdx, time)) &
                                    + uimsk(i,j,1)*(1 - uimsk(i,j,3))*(1 - uimsk(i,j,10))*          (ugrid(i  ,j-1) + dx*dudxBC(x_u(i+1) - halfdx, y_u(j-1), time)) &
                                    + (1 - uimsk(i,j,1))*(1 - uimsk(i,j,3))*(1 - uimsk(i,j,10))*    (ugrid(i  ,j  ) + dx*dudxBC(x_u(i+1) - halfdx, y_u(j  ), time) - dx*dudyBC(x_u(i+1), y_u(j-1) + halfdx, time))) &
                                    + (1 - ulmsk(i,j,10))*uBC(x_u(i+1), y_u(j-1), time) 

                            ! u(i-1,j+1) !
                            u_m1p1 = ulmsk(i,j,11)*(uimsk(i,j,11)*                                   ugrid(i-1,j+1) &
                                    + uimsk(i,j,2)*(1 - uimsk(i,j,11))*                             (ugrid(i-1,j  ) + dx*dudyBC(x_u(i-1), y_u(j+1) - halfdx, time)) &
                                    + uimsk(i,j,4)*(1 - uimsk(i,j,2))*(1 - uimsk(i,j,11))*          (ugrid(i  ,j+1) - dx*dudxBC(x_u(i-1) + halfdx, y_u(j+1), time)) &
                                    + (1 - uimsk(i,j,4))*(1 - uimsk(i,j,2))*(1 - uimsk(i,j,11))*    (ugrid(i  ,j  ) - dx*dudxBC(x_u(i-1) + halfdx, y_u(j  ), time) + dx*dudyBC(x_u(i-1), y_u(j+1) - halfdx, time))) &
                                    + (1 - ulmsk(i,j,11))*uBC(x_u(i-1), y_u(j+1), time)

                            ! u(i+1,j+1) !
                            u_p1p1 = ulmsk(i,j,12)*(uimsk(i,j,12)*                                   ugrid(i+1,j+1) &
                                    + uimsk(i,j,3)*(1 - uimsk(i,j,12))*                             (ugrid(i+1,j  ) + dx*dudyBC(x_u(i+1), y_u(j+1) - halfdx, time)) &
                                    + uimsk(i,j,4)*(1 - uimsk(i,j,3))*(1 - uimsk(i,j,12))*          (ugrid(i  ,j+1) + dx*dudxBC(x_u(i+1) - halfdx, y_u(j+1), time)) &
                                    + (1 - uimsk(i,j,4))*(1 - uimsk(i,j,3))*(1 - uimsk(i,j,12))*    (ugrid(i  ,j  ) + dx*dudxBC(x_u(i+1) - halfdx, y_u(j  ), time) + dx*dudyBC(x_u(i+1), y_u(j+1) - halfdx, time))) &
                                    + (1 - ulmsk(i,j,12))*uBC(x_u(i+1), y_u(j+1), time)

                        !===========================================!
                        !   V points needed for second derivs in V  !
                        !===========================================!

                            ! v(i-1,j-1) !
                            v_m1m1 = ulmsk(i,j,13)*(uimsk(i,j,13)*               vgrid(i-1,j-1) &
                                    + uimsk(i,j,14)*(1 - uimsk(i,j,13))*        (vgrid(i  ,j-1) - dx*dvdxBC(x_v(i-1) + halfdx, y_v(j-1), time))  &
                                    + (1 - uimsk(i,j,14))*(1 - uimsk(i,j,13))*  (v_m1p0         - dx*dvdyBC(x_v(i-1), y_v(j-1) + halfdx, time))) &
                                    + (1 - ulmsk(i,j,13))*vBC(x_v(i-1), y_v(j-1), time)

                            ! v(i,j-1) !
                            v_p0m1 = ulmsk(i,j,14)*(uimsk(i,j,14)*               vgrid(i  ,j-1) &
                                    + uimsk(i,j,13)*(1 - uimsk(i,j,14))*        (vgrid(i-1,j-1) + dx*dvdxBC(x_v(i) - halfdx, y_v(j-1), time))  &
                                    + (1 - uimsk(i,j,13))*(1 - uimsk(i,j,14))*  (v_p0p0         - dx*dvdyBC(x_v(i), y_v(j-1) + halfdx, time))) &
                                    + (1 - ulmsk(i,j,14))*vBC(x_v(i), y_v(j-1), time)

                            ! v(i-2,j) !
                            v_m2p0 = ulmsk(i,j,15)*(uimsk(i,j,15)*               vgrid(i-2,j  ) &
                                    + uimsk(i,j,17)*(1 - uimsk(i,j,15))*        (vgrid(i-2,j+1) - dx*dvdyBC(x_v(i-2), y_v(j) + halfdx, time))  &
                                    + (1 - uimsk(i,j,17))*(1 - uimsk(i,j,15))*  (v_m1p0         - dx*dvdxBC(x_v(i-2) + halfdx, y_v(j), time))) &
                                    + (1 - ulmsk(i,j,15))*vBC(x_v(i-2), y_v(j), time)

                            ! v(i+1,j) !
                            v_p1p0 = ulmsk(i,j,16)*(uimsk(i,j,16)*               vgrid(i+1,j  ) &
                                    + uimsk(i,j,18)*(1 - uimsk(i,j,16))*        (vgrid(i+1,j+1) - dx*dvdyBC(x_v(i+1), y_v(j) + halfdx, time))  &
                                    + (1 - uimsk(i,j,18))*(1 - uimsk(i,j,16))*  (v_p0p0         + dx*dvdxBC(x_v(i+1) - halfdx, y_v(j), time))) &
                                    + (1 - ulmsk(i,j,16))*vBC(x_v(i+1), y_v(j), time)

                            ! v(i-2,j+1) !
                            v_m2p1 = ulmsk(i,j,17)*(uimsk(i,j,17)*               vgrid(i-2,j+1) &
                                    + uimsk(i,j,15)*(1 - uimsk(i,j,17))*        (vgrid(i-2,j  ) + dx*dvdyBC(x_v(i-2), y_v(j+1) - halfdx, time))  &
                                    + (1 - uimsk(i,j,15))*(1 - uimsk(i,j,17))*  (v_m1p1         - dx*dvdxBC(x_v(i-2) + halfdx, y_v(j+1), time))) &
                                    + (1 - ulmsk(i,j,17))*vBC(x_v(i-2), y_v(j+1), time)

                            ! v(i+1,j+1) !
                            v_p1p1 = ulmsk(i,j,18)*(uimsk(i,j,18)*               vgrid(i+1,j+1) &
                                    + uimsk(i,j,16)*(1 - uimsk(i,j,18))*        (vgrid(i+1,j  ) + dx*dvdyBC(x_v(i+1), y_v(j+1) - halfdx, time))  &
                                    + (1 - uimsk(i,j,16))*(1 - uimsk(i,j,18))*  (v_p0p1         + dx*dvdxBC(x_v(i+1) - halfdx, y_v(j+1), time))) &
                                    + (1 - ulmsk(i,j,18))*vBC(x_v(i+1), y_v(j+1), time)

                            ! v(i-1,j+2) !
                            v_m1p2 = ulmsk(i,j,19)*(uimsk(i,j,19)*               vgrid(i-1,j+2) &
                                    + uimsk(i,j,20)*(1 - uimsk(i,j,19))*        (vgrid(i  ,j+2) - dx*dvdxBC(x_v(i-1) + halfdx, y_v(j+2), time))  &
                                    + (1 - uimsk(i,j,20))*(1 - uimsk(i,j,19))*  (v_m1p1         + dx*dvdyBC(x_v(i-1), y_v(j+2) - halfdx, time))) &
                                    + (1 - ulmsk(i,j,19))*vBC(x_v(i-1), y_v(j+2), time)

                            ! v(i,j+2) !
                            v_p0p2 = ulmsk(i,j,20)*(uimsk(i,j,20)*               vgrid(i  ,j+2) &
                                    + uimsk(i,j,19)*(1 - uimsk(i,j,20))*        (vgrid(i-1,j+2) + dx*dvdxBC(x_v(i) - halfdx, y_v(j+2), time))  &
                                    + (1 - uimsk(i,j,19))*(1 - uimsk(i,j,20))*  (v_p0p1         + dx*dvdyBC(x_v(i), y_v(j+2) - halfdx, time))) &
                                    + (1 - ulmsk(i,j,20))*vBC(x_v(i), y_v(j+2), time)

                    !===========================!
                    !   Calculate Derivatives   !
                    !===========================!
                        dudx    = cnst1*(u_p1p0 - u_m1p0)
                        dudy    = cnst1*(u_p0p1 - u_p0m1)
                        d2udx2  = cnst2*(u_p1p0 - 2*ugrid(i,j) + u_m1p0)
                        d2udy2  = cnst2*(u_p0p1 - 2*ugrid(i,j) + u_p0m1)
                        d2udxdy = (cnst2/4)*(u_p1p1 - u_m1p1 - u_p1m1 + u_m1m1)

                        dvdx    = cnst1*(v_p0p1 - v_m1p1 + v_p0p0 - v_m1p0)
                        dvdy    = cnst1*(v_p0p1 - v_p0p0 + v_m1p1 - v_m1p0)
                        d2vdx2  = (cnst2/4)*(v_p1p1 - v_p0p1 - v_m1p1 + v_m2p1 &
                                                + v_p1p0 - v_p0p0 - v_m1p0 + v_m2p0)
                        d2vdy2  = (cnst2/4)*(v_p0p2 - v_p0p1 - v_p0p0 + v_p0m1 &
                                                + v_m1p2 - v_m1p1 - v_m1p0 + v_m1m1)
                        d2vdxdy = cnst2*(v_p0p1 - v_p0p0 - v_m1p1 + v_m1p0)

                            ! Store for test purposes !
                            ! deriv_U(i,j) = dudy
                else
                    ! Masking isn't required !

                    !===========================!
                    !   Calculate Derivatives   !
                    !===========================!
                        dudx    = cnst1*(ugrid(i+1,j) - ugrid(i-1,j))
                        dudy    = cnst1*(ugrid(i,j+1) - ugrid(i,j-1))
                        d2udx2  = cnst2*(ugrid(i+1,j) - 2*ugrid(i,j) + ugrid(i-1,j))
                        d2udy2  = cnst2*(ugrid(i,j+1) - 2*ugrid(i,j) + ugrid(i,j-1))
                        d2udxdy = (cnst2/4)*(ugrid(i+1,j+1) - ugrid(i-1,j+1) - ugrid(i+1,j-1) + ugrid(i-1,j-1))

                        dvdx    = cnst1*(vgrid(i,j+1) - vgrid(i-1,j+1) + vgrid(i,j) - vgrid(i-1,j))
                        dvdy    = cnst1*(vgrid(i,j+1) - vgrid(i,j) + vgrid(i-1,j+1) - vgrid(i-1,j))
                        d2vdx2  = (cnst2/4)*(vgrid(i+1,j+1) - vgrid(i,j+1) - vgrid(i-1,j+1) + vgrid(i-2,j+1) &
                                                + vgrid(i+1,j) - vgrid(i,j) - vgrid(i-1,j) + vgrid(i-2,j))
                        d2vdy2  = (cnst2/4)*(vgrid(i,j+2) - vgrid(i,j+1) - vgrid(i,j) + vgrid(i,j-1) &
                                                + vgrid(i-1,j+2) - vgrid(i-1,j+1) - vgrid(i-1,j) + vgrid(i-1,j-1))
                        d2vdxdy = cnst2*(vgrid(i,j+1) - vgrid(i,j) - vgrid(i-1,j+1) + vgrid(i-1,j))

                            ! Store for test purposes !
                            ! deriv_U(i,j) = dudy

                end if 

                !===============================!
                !       Calculate Delta         !
                !===============================!
                    strain_11   = dudx
                    strain_22   = dvdy 
                    strain_12   = 0.5*(dudy + dvdx)

                    delta       = sqrt( (strain_11**2 + strain_22**2)*(1 + ellip_rat**(-2)) &
                                        + 4*(ellip_rat**(-2))*(strain_12**2)                &
                                        + 2*strain_11*strain_22*(1 - ellip_rat**(-2)) )

                !===================================!
                !  Calculate derivatives of Delta^2 !
                !===================================!
                    ddelta2_dx  = (1 + ellip_rat**(-2))*(2*strain_11*d2udx2 + 2*strain_22*d2vdxdy)  &
                                    + 4*(ellip_rat**(-2))*strain_12*(d2udxdy + d2vdx2)              &
                                    + 2*(1 - ellip_rat**(-2))*(strain_22*d2udx2 + strain_11*d2vdxdy)

                    ddelta2_dy  = (1 + ellip_rat**(-2))*(2*strain_11*d2udxdy + 2*strain_22*d2vdy2) &
                                    + 4*(ellip_rat**(-2))*strain_12*(d2udy2 + d2vdxdy) &
                                    + 2*(1 - ellip_rat**(-2))*(strain_22*d2udxdy + strain_11*d2vdy2)

                !===================================================!
                !    Calculate Viscosities and Necessary Gradients  !
                !===================================================!

                    cnst3   = (2*delta*maxvisc_cf + machine_eps)
                    cnst4   = (maxvisc_cf**2)*P_u(i,j)/(delta*(cnst3**2) + machine_eps)

                    dPdx    = (1./dx)*(P_T(i  ,j  ) - P_T(i-1,j  ))
                    dPdy    = (1./dx)*(P_N(i  ,j+1) - P_N(i  ,j  ))

                    zeta_u(i,j) = maxvisc_cf*P_u(i,j)*tanh(1./cnst3)
                    eta_u(i,j)  = zeta_u(i,j)*(ellip_rat**(-2))

                    zeta_gx_u(i,j)  = maxvisc_cf*tanh(1./cnst3)*dPdx - cnst4*(1 - (tanh(1./cnst3))**2)*ddelta2_dx
                    eta_gx_u(i,j)   = zeta_gx_u(i,j)*(ellip_rat**(-2))
                    eta_gy_u(i,j)   = (ellip_rat**(-2))*maxvisc_cf*tanh(1./cnst3)*dPdy - (ellip_rat**(-2))*cnst4*(1 - (tanh(1./cnst3))**2)*ddelta2_dy
            end do 

            !=======================!
            !       V points        !
            !=======================!
            do ij = 1, nV_pnts
                i = indxVi(ij)
                j = indxVj(ij)

                ! Check for Masking !
                if ((product(vlmsk(i,j,:)) == 0) .or. (product(vimsk(i,j,:)) == 0)) then

                    !===========================!
                    ! mask velocities as needed !
                    !===========================! 

                        !=======================!
                        !  Direct U Neighbours  !
                        !=======================!

                            ! u(i,j-1) !
                            u_p0m1 = vlmsk(i,j,1)*(vimsk(i,j,1)*                                 ugrid(i  ,j-1)  &
                                    + vimsk(i,j,3)*(1 - vimsk(i,j,1))*                          (ugrid(i  ,j  ) - dx*dudyBC(x_u(i), y_u(j) - halfdx, time)) &
                                    + vimsk(i,j,2)*(1 - vimsk(i,j,3))*(1 - vimsk(i,j,1))*       (ugrid(i+1,j-1) - dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time)) & 
                                    + (1 - vimsk(i,j,2))*(1 - vimsk(i,j,3))*(1 - vimsk(i,j,1))* (ugrid(i+1,j  ) - dx*dudxBC(x_u(i) + halfdx, y_u(j), time) - dx*dudyBC(x_u(i), y_u(j) - halfdx, time))) &
                                    + (1 - vlmsk(i,j,1))*uBC(x_u(i), y_u(j-1), time) 
                            
                            ! u(i+1,j-1) !
                            u_p1m1 = vlmsk(i,j,2)*(vimsk(i,j,2)*                                 ugrid(i+1,j-1)  &
                                    + vimsk(i,j,4)*(1 - vimsk(i,j,2))*                          (ugrid(i+1,j  ) - dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time)) & 
                                    + vimsk(i,j,1)*(1 - vimsk(i,j,4))*(1 - vimsk(i,j,2))*       (ugrid(i  ,j-1) + dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time)) &
                                    + (1 - vimsk(i,j,1))*(1 - vimsk(i,j,4))*(1 - vimsk(i,j,2))* (ugrid(i  ,j  ) + dx*dudxBC(x_u(i) + halfdx, y_u(j), time) - dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time))) &
                                    + (1 - vlmsk(i,j,2))*uBC(x_u(i+1), y_u(j-1), time)
                            
                            ! u(i,j) !
                            u_p0p0 = vlmsk(i,j,3)*(vimsk(i,j,3)*                                 ugrid(i  ,j  ) & 
                                    + vimsk(i,j,1)*(1 - vimsk(i,j,3))*                          (ugrid(i  ,j-1) + dx*dudyBC(x_u(i), y_u(j) - halfdx, time)) & 
                                    + vimsk(i,j,4)*(1 - vimsk(i,j,1))*(1 - vimsk(i,j,3))*       (ugrid(i+1,j  ) - dx*dudxBC(x_u(i) + halfdx, y_u(j), time)) &  
                                    + (1 - vimsk(i,j,4))*(1 - vimsk(i,j,1))*(1 - vimsk(i,j,3))* (ugrid(i+1,j-1) - dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time) + dx*dudyBC(x_u(i), y_u(j) - halfdx, time))) &
                                    + (1 - vlmsk(i,j,3))*uBC(x_u(i), y_u(j), time)
                            
                            ! u(i+1,j) !
                            u_p1p0 = vlmsk(i,j,4)*(vimsk(i,j,4)*                                 ugrid(i+1,j  ) & 
                                    + vimsk(i,j,2)*(1 - vimsk(i,j,4))*                          (ugrid(i+1,j-1) + dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time)) & 
                                    + vimsk(i,j,3)*(1 - vimsk(i,j,2))*(1 - vimsk(i,j,4))*       (ugrid(i  ,j  ) + dx*dudxBC(x_u(i) + halfdx, y_u(j), time)) & 
                                    + (1 - vimsk(i,j,3))*(1 - vimsk(i,j,2))*(1 - vimsk(i,j,4))* (ugrid(i  ,j-1) + dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time) + dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time))) &
                                    + (1 - vlmsk(i,j,4))*uBC(x_u(i+1), y_u(j), time) 

                        !=======================!
                        !  Direct V Neighbours  !
                        !=======================!
                            v_p0m1 = vlmsk(i,j,5)*(vimsk(i,j,5)*vgrid(i  ,j-1) + (1 - vimsk(i,j,5))*(vgrid(i,j) - dx*dvdyBC(x_v(i), y_v(j) - halfdx, time))) + (1 - vlmsk(i,j,5))*vBC(x_v(i  ), y_v(j-1), time)  
                            v_m1p0 = vlmsk(i,j,6)*(vimsk(i,j,6)*vgrid(i-1,j  ) + (1 - vimsk(i,j,6))*(vgrid(i,j) - dx*dvdxBC(x_v(i) - halfdx, y_v(j), time))) + (1 - vlmsk(i,j,6))*vBC(x_v(i-1), y_v(j  ), time)
                            v_p1p0 = vlmsk(i,j,7)*(vimsk(i,j,7)*vgrid(i+1,j  ) + (1 - vimsk(i,j,7))*(vgrid(i,j) + dx*dvdxBC(x_v(i) + halfdx, y_v(j), time))) + (1 - vlmsk(i,j,7))*vBC(x_v(i+1), y_v(j  ), time)
                            v_p0p1 = vlmsk(i,j,8)*(vimsk(i,j,8)*vgrid(i  ,j+1) + (1 - vimsk(i,j,8))*(vgrid(i,j) + dx*dvdyBC(x_v(i), y_v(j) + halfdx, time))) + (1 - vlmsk(i,j,8))*vBC(x_v(i  ), y_v(j+1), time) 

                        !============================================!
                        ! V points needed for mixed derivatives in V !
                        !============================================!

                            ! v(i-1,j-1) !
                            v_m1m1 = vlmsk(i,j,9)*(vimsk(i,j,9)*                                     vgrid(i-1,j-1) &
                                    + vimsk(i,j,5)*(1 - vimsk(i,j,9))*                              (vgrid(i  ,j-1) - dx*dvdxBC(x_v(i-1) + halfdx, y_v(j-1), time)) &
                                    + vimsk(i,j,6)*(1 - vimsk(i,j,5))*(1 - vimsk(i,j,9))*           (vgrid(i-1,j  ) - dx*dvdyBC(x_v(i-1), y_v(j-1) + halfdx, time)) &
                                    + (1 - vimsk(i,j,6))*(1 - vimsk(i,j,5))*(1 - vimsk(i,j,9))*     (vgrid(i  ,j  ) - dx*dvdxBC(x_v(i-1) + halfdx, y_v(j  ), time) - dx*dvdyBC(x_v(i-1), y_v(j-1) + halfdx, time))) &
                                    + (1 - vlmsk(i,j,9))*vBC(x_v(i-1), y_v(j-1), time)

                            ! v(i+1,j-1) !
                            v_p1m1 = vlmsk(i,j,10)*(vimsk(i,j,10)*                                   vgrid(i+1,j-1) &
                                    + vimsk(i,j,5)*(1 - vimsk(i,j,10))*                             (vgrid(i  ,j-1) + dx*dvdxBC(x_v(i+1) - halfdx, y_v(j-1), time)) &
                                    + vimsk(i,j,7)*(1 - vimsk(i,j,5))*(1 - vimsk(i,j,10))*          (vgrid(i+1,j  ) - dx*dvdyBC(x_v(i+1), y_v(j-1) + halfdx, time)) &
                                    + (1 - vimsk(i,j,7))*(1 - vimsk(i,j,5))*(1 - vimsk(i,j,10))*    (vgrid(i  ,j  ) + dx*dvdxBC(x_v(i+1) - halfdx, y_v(j  ), time) - dx*dvdyBC(x_v(i+1), y_v(j-1) + halfdx, time))) &
                                    + (1 - vlmsk(i,j,10))*vBC(x_v(i+1), y_v(j-1), time)

                            ! v(i-1,j+1) !
                            v_m1p1 = vlmsk(i,j,11)*(vimsk(i,j,11)*                                   vgrid(i-1,j+1) &
                                    + vimsk(i,j,8)*(1 - vimsk(i,j,11))*                             (vgrid(i  ,j+1) - dx*dvdxBC(x_v(i-1) + halfdx, y_v(j+1), time)) &
                                    + vimsk(i,j,6)*(1 - vimsk(i,j,8))*(1 - vimsk(i,j,11))*          (vgrid(i-1,j  ) + dx*dvdyBC(x_v(i-1), y_v(j+1) - halfdx, time)) &
                                    + (1 - vimsk(i,j,6))*(1 - vimsk(i,j,8))*(1 - vimsk(i,j,11))*    (vgrid(i  ,j  ) - dx*dvdxBC(x_v(i-1) + halfdx, y_v(j  ), time) + dx*dvdyBC(x_v(i-1), y_v(j+1) - halfdx, time))) &
                                    + (1 - vlmsk(i,j,11))*vBC(x_v(i-1), y_v(j+1), time)

                            ! v(i+1,j+1) !
                            v_p1p1 = vlmsk(i,j,12)*(vimsk(i,j,12)*                                   vgrid(i+1,j+1) &
                                    + vimsk(i,j,8)*(1 - vimsk(i,j,12))*                             (vgrid(i  ,j+1) + dx*dvdxBC(x_v(i+1) - halfdx, y_v(j+1), time)) &
                                    + vimsk(i,j,7)*(1 - vimsk(i,j,8))*(1 - vimsk(i,j,12))*          (vgrid(i+1,j  ) + dx*dvdyBC(x_v(i+1), y_v(j+1) - halfdx, time)) &
                                    + (1 - vimsk(i,j,7))*(1 - vimsk(i,j,8))*(1 - vimsk(i,j,12))*    (vgrid(i  ,j  ) + dx*dvdxBC(x_v(i+1) - halfdx, y_v(j  ), time) + dx*dvdyBC(x_v(i+1), y_v(j+1) - halfdx, time))) &
                                    + (1 - vlmsk(i,j,12))*vBC(x_v(i+1), y_v(j+1), time)

                        !===========================================!
                        !   U points needed for second derivs in U  !
                        !===========================================!

                            ! u(i,j-2) !
                            u_p0m2 = vlmsk(i,j,13)*(vimsk(i,j,13)*               ugrid(i  ,j-2) &
                                    + vimsk(i,j,14)*(1 - vimsk(i,j,13))*        (ugrid(i+1,j-2) - dx*dudxBC(x_u(i) + halfdx, y_u(j-2), time))  &
                                    + (1 - vimsk(i,j,14))*(1 - vimsk(i,j,13))*  (u_p0m1         - dx*dudyBC(x_u(i), y_u(j-2) + halfdx, time))) &
                                    + (1 - vlmsk(i,j,13))*uBC(x_u(i), y_u(j-2), time)

                            ! u(i+1,j-2) !
                            u_p1m2 = vlmsk(i,j,14)*(vimsk(i,j,14)*               ugrid(i+1,j-2) &
                                    + vimsk(i,j,13)*(1 - vimsk(i,j,14))*        (ugrid(i  ,j-2) + dx*dudxBC(x_u(i+1) - halfdx, y_u(j-2), time))  &
                                    + (1 - vimsk(i,j,13))*(1 - vimsk(i,j,14))*  (u_p1m1         - dx*dudyBC(x_u(i+1), y_u(j-2) + halfdx, time))) &
                                    + (1 - vlmsk(i,j,14))*uBC(x_u(i+1), y_u(j-2), time)

                            ! u(i-1,j-1) !
                            u_m1m1 = vlmsk(i,j,15)*(vimsk(i,j,15)*               ugrid(i-1,j-1) &
                                    + vimsk(i,j,17)*(1 - vimsk(i,j,15))*        (ugrid(i-1,j  ) - dx*dudyBC(x_u(i-1), y_u(j-1) + halfdx, time))  &
                                    + (1 - vimsk(i,j,17))*(1 - vimsk(i,j,15))*  (u_p0m1         - dx*dudxBC(x_u(i-1) + halfdx, y_u(j-1), time))) &
                                    + (1 - vlmsk(i,j,15))*uBC(x_u(i-1), y_u(j-1), time)

                            ! u(i+2,j-1) !
                            u_p2m1 = vlmsk(i,j,16)*(vimsk(i,j,16)*               ugrid(i+2,j-1) &
                                    + vimsk(i,j,18)*(1 - vimsk(i,j,16))*        (ugrid(i+2,j  ) - dx*dudyBC(x_u(i+2), y_u(j-1) + halfdx, time))  &
                                    + (1 - vimsk(i,j,18))*(1 - vimsk(i,j,16))*  (u_p1m1         + dx*dudxBC(x_u(i+2) - halfdx, y_u(j-1), time))) &
                                    + (1 - vlmsk(i,j,16))*uBC(x_u(i+2), y_u(j-1), time)

                            ! u(i-1,j) !
                            u_m1p0 = vlmsk(i,j,17)*(vimsk(i,j,17)*               ugrid(i-1,j  ) &
                                    + vimsk(i,j,15)*(1 - vimsk(i,j,17))*        (ugrid(i-1,j-1) + dx*dudyBC(x_u(i-1), y_u(j) - halfdx, time))  &
                                    + (1 - vimsk(i,j,15))*(1 - vimsk(i,j,17))*  (u_p0p0         - dx*dudxBC(x_u(i-1) + halfdx, y_u(j), time))) &
                                    + (1 - vlmsk(i,j,17))*uBC(x_u(i-1), y_u(j), time)

                            ! u(i+2,j) !
                            u_p2p0 = vlmsk(i,j,18)*(vimsk(i,j,18)*                ugrid(i+2,j  ) &
                                    + vimsk(i,j,16)*(1 - vimsk(i,j,18))*         (ugrid(i+2,j-1) + dx*dudyBC(x_u(i+2), y_u(j) - halfdx, time)) &
                                    + (1 - vimsk(i,j,16))*(1 - vimsk(i,j,18))*   (u_p1p0         + dx*dudxBC(x_u(i+2) - halfdx, y_u(j), time))) &
                                    + (1 - vlmsk(i,j,18))*uBC(x_u(i+2), y_u(j), time)

                            ! u(i,j+1) !
                            u_p0p1 = vlmsk(i,j,19)*(vimsk(i,j,19)*               ugrid(i  ,j+1) &
                                    + vimsk(i,j,20)*(1 - vimsk(i,j,19))*        (ugrid(i+1,j+1) - dx*dudxBC(x_u(i) + halfdx, y_u(j+1), time))  & 
                                    + (1 - vimsk(i,j,20))*(1 - vimsk(i,j,19))*  (u_p0p0         + dx*dudyBC(x_u(i), y_u(j+1) - halfdx, time))) &
                                    + (1 - vlmsk(i,j,19))*uBC(x_u(i), y_u(j+1), time)

                            ! u(i+1,j+1) !
                            u_p1p1 = vlmsk(i,j,20)*(vimsk(i,j,20)*               ugrid(i+1,j+1) &
                                    + vimsk(i,j,19)*(1 - vimsk(i,j,20))*        (ugrid(i  ,j+1) + dx*dudxBC(x_u(i+1) - halfdx, y_u(j+1), time))  &
                                    + (1 - vimsk(i,j,19))*(1 - vimsk(i,j,20))*  (u_p1p0         + dx*dudyBC(x_u(i+1), y_u(j+1) - halfdx, time))) &
                                    + (1 - vlmsk(i,j,20))*uBC(x_u(i+1), y_u(j+1), time)

                    !===========================!
                    !   Calculate Derivatives   !
                    !===========================!

                        dudx    = cnst1*(u_p1p0 - u_p0p0 + u_p1m1 - u_p0m1)
                        dudy    = cnst1*(u_p1p0 - u_p1m1 + u_p0p0 - u_p0m1)
                        d2udx2  = (cnst2/4)*(u_p2p0 - u_p1p0 - u_p0p0 + u_m1p0 &
                                                + u_p2m1 - u_p1m1 - u_p0m1 + u_m1m1)
                        d2udy2  = (cnst2/4)*(u_p1p1 - u_p1p0 - u_p1m1 + u_p1m2 &
                                                + u_p0p1 - u_p0p0 - u_p0m1 + u_p0m2)
                        d2udxdy = cnst2*(u_p1p0 - u_p0p0 - u_p1m1 + u_p0m1)

                        dvdx    = cnst1*(v_p1p0 - v_m1p0)
                        dvdy    = cnst1*(v_p0p1 - v_p0m1)
                        d2vdx2  = cnst2*(v_p1p0 - 2*vgrid(i,j) + v_m1p0)
                        d2vdy2  = cnst2*(v_p0p1 - 2*vgrid(i,j) + v_p0m1)
                        d2vdxdy = (cnst2/4)*(v_p1p1 - v_m1p1 - v_p1m1 + v_m1m1)

                            ! Store for test purposes !
                            ! deriv_V(i,j) = dudy
                else
                    ! Masking isn't required !

                    !===========================!
                    !   Calculate Derivatives   !
                    !===========================!
                        dudx    = cnst1*(ugrid(i+1,j) - ugrid(i,j) + ugrid(i+1,j-1) - ugrid(i,j-1))
                        dudy    = cnst1*(ugrid(i+1,j) - ugrid(i+1,j-1) + ugrid(i,j) - ugrid(i,j-1))
                        d2udx2  = (cnst2/4)*(ugrid(i+2,j) - ugrid(i+1,j) - ugrid(i,j) + ugrid(i-1,j) &
                                                + ugrid(i+2,j-1) - ugrid(i+1,j-1) - ugrid(i,j-1) + ugrid(i-1,j-1))
                        d2udy2  = (cnst2/4)*(ugrid(i+1,j+1) - ugrid(i+1,j) - ugrid(i+1,j-1) + ugrid(i+1,j-2) &
                                                + ugrid(i,j+1) - ugrid(i,j) - ugrid(i,j-1) + ugrid(i,j-2))
                        d2udxdy = cnst2*(ugrid(i+1,j) - ugrid(i,j) - ugrid(i+1,j-1) + ugrid(i,j-1))

                        dvdx    = cnst1*(vgrid(i+1,j) - vgrid(i-1,j))
                        dvdy    = cnst1*(vgrid(i,j+1) - vgrid(i,j-1))
                        d2vdx2  = cnst2*(vgrid(i+1,j) - 2*vgrid(i,j) + vgrid(i-1,j))
                        d2vdy2  = cnst2*(vgrid(i,j+1) - 2*vgrid(i,j) + vgrid(i,j-1))
                        d2vdxdy = (cnst2/4)*(vgrid(i+1,j+1) - vgrid(i-1,j+1) - vgrid(i+1,j-1) + vgrid(i-1,j-1))

                            ! Store for test purposes !
                            ! deriv_V(i,j) = dudy
                end if

                !===============================!
                !       Calculate Delta         !
                !===============================!
                    strain_11   = dudx
                    strain_22   = dvdy 
                    strain_12   = 0.5*(dudy + dvdx)

                    delta       = sqrt( (strain_11**2 + strain_22**2)*(1 + ellip_rat**(-2)) &
                                        + 4*(ellip_rat**(-2))*(strain_12**2)                &
                                        + 2*strain_11*strain_22*(1 - ellip_rat**(-2)) )

                !===================================!
                !  Calculate derivatives of Delta^2 !
                !===================================!
                    ddelta2_dx  = (1 + ellip_rat**(-2))*(2*strain_11*d2udx2 + 2*strain_22*d2vdxdy)  &
                                    + 4*(ellip_rat**(-2))*strain_12*(d2udxdy + d2vdx2)              &
                                    + 2*(1 - ellip_rat**(-2))*(strain_22*d2udx2 + strain_11*d2vdxdy)

                    ddelta2_dy  = (1 + ellip_rat**(-2))*(2*strain_11*d2udxdy + 2*strain_22*d2vdy2) &
                                    + 4*(ellip_rat**(-2))*strain_12*(d2udy2 + d2vdxdy) &
                                    + 2*(1 - ellip_rat**(-2))*(strain_22*d2udxdy + strain_11*d2vdy2)

                !===================================================!
                !    Calculate Viscosities and Necessary Gradients  !
                !===================================================!
                    cnst3   = 2*delta*maxvisc_cf + machine_eps
                    cnst4   = (maxvisc_cf**2)*P_v(i,j)/(delta*(cnst3**2) + machine_eps)

                    dPdx    = (1./dx)*(P_N(i+1,j  ) - P_N(i  ,j  ))
                    dPdy    = (1./dx)*(P_T(i  ,j  ) - P_T(i  ,j-1))

                    zeta_v(i,j) = maxvisc_cf*P_v(i,j)*tanh(1./cnst3)
                    eta_v(i,j)  = zeta_v(i,j)*(ellip_rat**(-2))

                    eta_gx_v(i,j)   = (ellip_rat**(-2))*maxvisc_cf*tanh(1./cnst3)*dPdx - (ellip_rat**(-2))*cnst4*(1 - (tanh(1./cnst3))**2)*ddelta2_dx
                    zeta_gy_v(i,j)  = maxvisc_cf*tanh(1./cnst3)*dPdy - cnst4*(1 - (tanh(1./cnst3))**2)*ddelta2_dy
                    eta_gy_v(i,j)   = zeta_gy_v(i,j)*(ellip_rat**(-2))
            end do

            !===========================!
            !   Save Files for testing  !
            !===========================!
                ! open(unit = 1, file = "./output/dudy_U"//trim(dx_f)//".dat", status = "unknown")
                ! open(unit = 2, file = "./output/dudy_V"//trim(dx_f)//".dat", status = "unknown")
                ! call csv_write_dble_2d(1, deriv_U)
                ! call csv_write_dble_2d(2, deriv_V)
                ! close(1)
                ! close(2)

                ! open(unit = 1, file = "../Valid_Pblm/ViscGrads_Tests/dzdxU"//trim(dx_f)//".dat", status = "unknown")
                ! open(unit = 2, file = "../Valid_Pblm/ViscGrads_Tests/dedxU"//trim(dx_f)//".dat", status = "unknown")
                ! open(unit = 3, file = "../Valid_Pblm/ViscGrads_Tests/dedyU"//trim(dx_f)//".dat", status = "unknown")
                ! call csv_write_dble_2d(1, zeta_gx_u)
                ! call csv_write_dble_2d(2, eta_gx_u)
                ! call csv_write_dble_2d(3, eta_gy_u)
                ! close(1)
                ! close(2)
                ! close(3)

                ! open(unit = 1, file = "../Valid_Pblm/ViscGrads_Tests/dzdyV"//trim(dx_f)//".dat", status = "unknown")
                ! open(unit = 2, file = "../Valid_Pblm/ViscGrads_Tests/dedxV"//trim(dx_f)//".dat", status = "unknown")
                ! open(unit = 3, file = "../Valid_Pblm/ViscGrads_Tests/dedyV"//trim(dx_f)//".dat", status = "unknown")
                ! call csv_write_dble_2d(1, zeta_gy_v)
                ! call csv_write_dble_2d(2, eta_gx_v)
                ! call csv_write_dble_2d(3, eta_gy_v)
                ! close(1)
                ! close(2)
                ! close(3)

                ! open(unit = 1, file = "../Valid_Pblm/Visc_Tests/zU"//trim(dx_f)//".dat", status = "unknown")
                ! open(unit = 2, file = "../Valid_Pblm/Visc_Tests/eU"//trim(dx_f)//".dat", status = "unknown")
                ! open(unit = 3, file = "../Valid_Pblm/Visc_Tests/zV"//trim(dx_f)//".dat", status = "unknown")
                ! open(unit = 4, file = "../Valid_Pblm/Visc_Tests/eV"//trim(dx_f)//".dat", status = "unknown")
                ! call csv_write_dble_2d(1, zeta_u)
                ! call csv_write_dble_2d(2, eta_u)
                ! call csv_write_dble_2d(3, zeta_v)
                ! call csv_write_dble_2d(4, eta_v)
                ! close(1)
                ! close(2)
                ! close(3)
                ! close(4)

    end subroutine calc_visc_and_viscgrads

!===========================================================================================!
!                               Calculate viscosities (T and N points)                      !
!===========================================================================================!
    subroutine calc_visc(zeta_T, zeta_N, eta_T, eta_N, P_T, P_N, ugrid, vgrid, time, &
                        Tlmsk, Nlmsk, Timsk, Nimsk, indxTi, indxTj, indxNi, indxNj, &
                        nT_pnts, nN_pnts, nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV)
        !=======================================================================================
        ! This subroutine calculates the bulk and shear viscosities at the T points and N points.
        !
        ! Before the viscosities are calculated, the strain rates are calculated according
        ! to ugrid and vgrid, using computational masks to set the boundary conditions. 
        !
        ! NOTE: 
        !       -   For now, we are assuming that at least one of the nearest u's and nearest
        !           v's (to the respective tracer or node point) are contained in ice. I have 
        !           programmed a warning to alert the user if this isn't the case.
        !
        ! THIS ROUTINE IS NOW OLD AND OUT OF DATE AND SHOULD BE REMOVED ONCE THE NEW DISCRETIZATION
        ! IS FUNCITONAL.
        !
        !=======================================================================================

        !==================!
        !------- In--------!
        !==================!
        integer(kind = int_kind), intent(in) :: &
            nxT, nyT, nxN, nyN, nxU, nyU, nxV, nyV,     &   ! limit of indicies for all four point types
            nT_pnts, nN_pnts                                ! number of T and N points with-in the computational domain
        integer(kind = int_kind), dimension(nT_pnts) :: &
            indxTi, indxTj                                  ! locations of T points in the comp. domain
        integer(kind = int_kind), dimension(nN_pnts) :: &
            indxNi, indxNj                                  ! locations of N points in the comp. domain
        integer(kind = int_kind), dimension (1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T, 12), intent(in) :: &
            Tlmsk, Timsk                                    ! computational masks for calculating delta at T points
        integer(kind = int_kind), dimension (nxN, nyN, 12), intent(in) :: &
            Nlmsk, Nimsk                                    ! computational masks for calculating delta at N points

        real(kind = dbl_kind), intent(in) :: &
            time                                            ! current time level
        real(kind = dbl_kind), dimension(1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U), intent(in) :: &
            ugrid                                           ! horizontal velocity on the grid 
        real(kind = dbl_kind), dimension(1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V), intent(in) :: &
            vgrid                                           ! vertical velocity on the grid
        real(kind = dbl_kind), dimension(1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent(in) :: &
            P_T                                             ! ice strength at T points
        real(kind = dbl_kind), dimension(nxN, nyN), intent(in) :: &
            P_N 

        !==================!
        !----- In/out -----!
        !==================!
        real(kind = dbl_kind), dimension(1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent(inout) :: &
            zeta_T, eta_T                                   ! bulk and shear viscosities at T points
        real(kind = dbl_kind), dimension(nxN, nyN), intent(inout) :: &
            zeta_N, eta_N                                   ! bulk and shear viscosites at N points

        !==================!
        !-- Local Vars ----!
        !==================!
        real(kind = dbl_kind) :: &
            u_p1p0, u_p0p0, u_p0p1, u_p0m1,             &   ! masked velocity components needed for delta calculation 
            u_p1p1, u_p1m1, u_m1p0, u_m1m1,             &                           
            v_p0p1, v_p0p0, v_p1p1, v_m1p1,             &             
            v_p1p0, v_m1p0, v_m1m1, v_p0m1,             &              
            strain_11, strain_12, strain_22,            &   ! strain rates, du/dx, dv/dy, 0.5(du/dy + dv/dx), respectively
            zeta_max,  delta,                           &   ! maximum bulk viscous coefficient    
            u_p1p0Tst, u_p0p0Tst, u_p0p1Tst, u_p0m1Tst, &   ! masked velocities used to test BC calcs
            u_p1p1Tst, u_p1m1Tst, u_m1p0Tst, u_m1m1Tst, &                           
            v_p0p1Tst, v_p0p0Tst, v_p1p1Tst, v_m1p1Tst, &             
            v_p1p0Tst, v_m1p0Tst, v_m1m1Tst, v_p0m1Tst

        integer(kind = int_kind) :: & 
            i, j, ij                                        ! local indices

        !-------- Calculate Viscosities at T points --------------!
        do ij = 1, nT_pnts
            i = indxTi(ij)
            j = indxTj(ij)

            ! ------ Strain Rates -------- !
            ! NOTE: at some point, it may be worth while to store these values in a matrix at each timestep
            ! NOTE: strain_12 ((1/2)(du/dy + dv/dx)) has to be interpolated at T points

            if ((product(Timsk(i,j,:)) == 0) .or. (product(Tlmsk(i,j,:)) == 0)) then 
                ! masking is required !

                ! inner ring !
                ! u_p1p0Tst = Tlmsk(i,j,5 )*(Timsk(i,j,5 )*ugrid(i+1,j  ) + (1 - Timsk(i,j,5 ))*ugrid(i  ,j  ))
                ! u_p0p0Tst = Tlmsk(i,j,2 )*(Timsk(i,j,2 )*ugrid(i  ,j  ) + (1 - Timsk(i,j,2 ))*ugrid(i+1,j  )) 
                ! v_p0p1Tst = Tlmsk(i,j,8 )*(Timsk(i,j,8 )*vgrid(i  ,j+1) + (1 - Timsk(i,j,8 ))*vgrid(i  ,j  )) 
                ! v_p0p0Tst = Tlmsk(i,j,11)*(Timsk(i,j,11)*vgrid(i  ,j  ) + (1 - Timsk(i,j,11))*vgrid(i  ,j+1))

                ! ! outer ring !
                ! u_p0p1Tst = Tlmsk(i,j,1 )*(Timsk(i,j,1 )*ugrid(i  ,j+1) + Timsk(i,j,4 )*(1 - Timsk(i,j,1 ))*ugrid(i+1,j+1) + (1 - Timsk(i,j,4 ))*(1 - Timsk(i,j,1 ))*u_p0p0Tst)
                ! u_p0m1Tst = Tlmsk(i,j,3 )*(Timsk(i,j,3 )*ugrid(i  ,j-1) + Timsk(i,j,6 )*(1 - Timsk(i,j,3 ))*ugrid(i+1,j-1) + (1 - Timsk(i,j,6 ))*(1 - Timsk(i,j,3 ))*u_p0p0Tst)
                ! u_p1p1Tst = Tlmsk(i,j,4 )*(Timsk(i,j,4 )*ugrid(i+1,j+1) + Timsk(i,j,1 )*(1 - Timsk(i,j,4 ))*ugrid(i  ,j+1) + (1 - Timsk(i,j,1 ))*(1 - Timsk(i,j,4 ))*u_p1p0Tst)
                ! u_p1m1Tst = Tlmsk(i,j,6 )*(Timsk(i,j,6 )*ugrid(i+1,j-1) + Timsk(i,j,3 )*(1 - Timsk(i,j,6 ))*ugrid(i  ,j-1) + (1 - Timsk(i,j,3 ))*(1 - Timsk(i,j,6 ))*u_p1p0Tst)
                ! v_p1p1Tst = Tlmsk(i,j,9 )*(Timsk(i,j,9 )*vgrid(i+1,j+1) + Timsk(i,j,12)*(1 - Timsk(i,j,9 ))*vgrid(i+1,j  ) + (1 - Timsk(i,j,12))*(1 - Timsk(i,j,9 ))*v_p0p1Tst)
                ! v_m1p1Tst = Tlmsk(i,j,7 )*(Timsk(i,j,7 )*vgrid(i-1,j+1) + Timsk(i,j,10)*(1 - Timsk(i,j,7 ))*vgrid(i-1,j  ) + (1 - Timsk(i,j,10))*(1 - Timsk(i,j,7 ))*v_p0p1Tst)
                ! v_p1p0Tst = Tlmsk(i,j,12)*(Timsk(i,j,12)*vgrid(i+1,j  ) + Timsk(i,j,9 )*(1 - Timsk(i,j,12))*vgrid(i+1,j+1) + (1 - Timsk(i,j,9 ))*(1 - Timsk(i,j,12))*v_p0p0Tst)
                ! v_m1p0Tst = Tlmsk(i,j,10)*(Timsk(i,j,10)*vgrid(i-1,j  ) + Timsk(i,j,7 )*(1 - Timsk(i,j,10))*vgrid(i-1,j+1) + (1 - Timsk(i,j,7 ))*(1 - Timsk(i,j,10))*v_p0p0Tst)

                ! inner ring !
                u_p1p0 = Tlmsk(i,j,5 )*(Timsk(i,j,5 )*ugrid(i+1,j  ) + (1 - Timsk(i,j,5 ))*(ugrid(i  ,j  ) + dx*dudxBC(x_T(i), y_T(j), time))) + (1 - Tlmsk(i,j,5 ))*uBC(x_u(i+1), y_u(j  ), time)
                u_p0p0 = Tlmsk(i,j,2 )*(Timsk(i,j,2 )*ugrid(i  ,j  ) + (1 - Timsk(i,j,2 ))*(ugrid(i+1,j  ) - dx*dudxBC(x_T(i), y_T(j), time))) + (1 - Tlmsk(i,j,2 ))*uBC(x_u(i  ), y_u(j  ), time) 
                v_p0p1 = Tlmsk(i,j,8 )*(Timsk(i,j,8 )*vgrid(i  ,j+1) + (1 - Timsk(i,j,8 ))*(vgrid(i  ,j  ) + dx*dvdyBC(x_T(i), y_T(j), time))) + (1 - Tlmsk(i,j,8 ))*vBC(x_v(i  ), y_v(j+1), time) 
                v_p0p0 = Tlmsk(i,j,11)*(Timsk(i,j,11)*vgrid(i  ,j  ) + (1 - Timsk(i,j,11))*(vgrid(i  ,j+1) - dx*dvdyBC(x_T(i), y_T(j), time))) + (1 - Tlmsk(i,j,11))*vBC(x_v(i  ), y_v(j  ), time)

                ! outer ring !
                ! u(i,j+1) !
                u_p0p1 = Tlmsk(i,j,1 )*(Timsk(i,j,1 )*           ugrid(i  ,j+1) &
                    + Timsk(i,j,4 )*(1 - Timsk(i,j,1 ))*        (ugrid(i+1,j+1) - dx*dudxBC(x_T(i  ), y_T(j+1), time)) & 
                    + (1 - Timsk(i,j,4 ))*(1 - Timsk(i,j,1 ))*  (u_p0p0         + dx*dudyBC(x_N(i  ), y_N(j+1), time))) &
                    + (1 - Tlmsk(i,j,1))*uBC(x_u(i), y_u(j+1), time)

                ! u(i,j-1) !
                u_p0m1 = Tlmsk(i,j,3 )*(Timsk(i,j,3 )*           ugrid(i  ,j-1) &
                    + Timsk(i,j,6 )*(1 - Timsk(i,j,3 ))*        (ugrid(i+1,j-1) - dx*dudxBC(x_T(i  ), y_T(j-1), time)) & 
                    + (1 - Timsk(i,j,6 ))*(1 - Timsk(i,j,3 ))*  (u_p0p0         - dx*dudyBC(x_N(i  ), y_N(j  ), time))) &
                    + (1 - Tlmsk(i,j,3))*uBC(x_u(i), y_u(j-1), time)

                ! u(i+1,j+1) !
                u_p1p1 = Tlmsk(i,j,4 )*(Timsk(i,j,4 )*           ugrid(i+1,j+1) &
                    + Timsk(i,j,1 )*(1 - Timsk(i,j,4 ))*        (ugrid(i  ,j+1) + dx*dudxBC(x_T(i  ), y_T(j+1), time)) & 
                    + (1 - Timsk(i,j,1 ))*(1 - Timsk(i,j,4 ))*  (u_p1p0        + dx*dudyBC(x_N(i+1), y_N(j+1), time))) &
                    + (1 - Tlmsk(i,j,4))*uBC(x_u(i+1), y_u(j+1), time)

                ! u(i+1,j-1) !
                u_p1m1 = Tlmsk(i,j,6 )*(Timsk(i,j,6 )*           ugrid(i+1,j-1) &
                    + Timsk(i,j,3 )*(1 - Timsk(i,j,6 ))*        (ugrid(i  ,j-1) + dx*dudxBC(x_T(i  ), y_T(j-1), time)) & 
                    + (1 - Timsk(i,j,3 ))*(1 - Timsk(i,j,6 ))*  (u_p1p0         - dx*dudyBC(x_N(i+1), y_N(j  ), time))) &
                    + (1 - Tlmsk(i,j,6))*uBC(x_u(i+1), y_u(j-1), time)

                ! v(i+1,j+1) !
                v_p1p1 = Tlmsk(i,j,9 )*(Timsk(i,j,9 )*           vgrid(i+1,j+1) &
                    + Timsk(i,j,12)*(1 - Timsk(i,j,9 ))*        (vgrid(i+1,j  ) + dx*dvdyBC(x_T(i+1), y_T(j  ), time)) & 
                    + (1 - Timsk(i,j,12))*(1 - Timsk(i,j,9 ))*  (v_p0p1         + dx*dvdxBC(x_N(i+1), y_N(j+1), time))) &
                    + (1 - Tlmsk(i,j,9))*vBC(x_v(i+1), y_v(j+1), time)

                ! v(i-1,j+1) !
                v_m1p1 = Tlmsk(i,j,7 )*(Timsk(i,j,7 )*           vgrid(i-1,j+1)& 
                    + Timsk(i,j,10)*(1 - Timsk(i,j,7 ))*        (vgrid(i-1,j  ) + dx*dvdyBC(x_T(i-1), y_T(j  ), time)) & 
                    + (1 - Timsk(i,j,10))*(1 - Timsk(i,j,7 ))*  (v_p0p1         - dx*dvdxBC(x_N(i  ), y_N(j+1), time))) &
                    + (1 - Tlmsk(i,j,7 ))*vBC(x_v(i-1), y_v(j+1), time)

                ! v(i+1,j) !
                v_p1p0 = Tlmsk(i,j,12)*(Timsk(i,j,12)*           vgrid(i+1,j  )& 
                    + Timsk(i,j,9 )*(1 - Timsk(i,j,12))*        (vgrid(i+1,j+1) - dx*dvdyBC(x_T(i+1), y_T(j  ), time)) & 
                    + (1 - Timsk(i,j,9 ))*(1 - Timsk(i,j,12))*  (v_p0p0         + dx*dvdxBC(x_N(i+1), y_N(j  ), time))) &
                    + (1 - Tlmsk(i,j,12))*vBC(x_v(i+1), y_v(j), time)

                ! v(i-1,j) !
                v_m1p0 = Tlmsk(i,j,10)*(Timsk(i,j,10)*           vgrid(i-1,j  )& 
                    + Timsk(i,j,7 )*(1 - Timsk(i,j,10))*        (vgrid(i-1,j+1) - dx*dvdyBC(x_T(i-1), y_T(j  ), time)) & 
                    + (1 - Timsk(i,j,7 ))*(1 - Timsk(i,j,10))*  (v_p0p0         - dx*dvdxBC(x_N(i  ), y_N(j  ), time))) &
                    + (1 - Tlmsk(i,j,10))*vBC(x_v(i-1), y_v(j), time)

                ! if (((u_p1p0Tst - u_p1p0) /= 0) .or. ((u_p0p0Tst - u_p0p0) /= 0) .or. ((v_p0p1Tst - v_p0p1) /= 0) .or. ((v_p0p0Tst - v_p0p0) /= 0) .or. ((u_p0p1Tst - u_p0p1) /= 0) .or. ((u_p0m1Tst - u_p0m1) /= 0) .or. &
                !     ((u_p1p1Tst - u_p1p1) /= 0) .or. ((u_p1m1Tst - u_p1m1) /= 0) .or. ((v_p1p1Tst - v_p1p1) /= 0) .or. ((v_m1p1Tst - v_m1p1) /= 0) .or. ((v_p1p0Tst - v_p1p0) /= 0) .or. ((v_m1p0Tst - v_m1p0) /= 0)) then 
                !     print *, "Masking error in T point calculations"
                ! end if 

                strain_11 = (u_p1p0 - u_p0p0)/dx                            ! (du/dx)
                strain_22 = (v_p0p1 - v_p0p0)/dx                            ! (dv/dy)
                strain_12 = (u_p0p1 - u_p0m1 + u_p1p1 - u_p1m1 + &          ! ((1/2)(du/dy + dv/dx))
                                v_p1p1 - v_m1p1 + v_p1p0 - v_m1p0)/(8*dx)

            else
                ! masking isn't required !

                strain_11 = (ugrid(i+1,j) - ugrid(i,j))/dx                                          ! (du/dx)
                strain_22 = (vgrid(i,j+1) - vgrid(i,j))/dx                                          ! (dv/dy)
                strain_12 = (ugrid(i,j+1) - ugrid(i,j-1) + ugrid(i+1,j+1) - ugrid(i+1,j-1) + &      ! ((1/2)(du/dy + dv/dx))
                                vgrid(i+1,j+1) - vgrid(i-1,j+1) + vgrid(i+1,j) - vgrid(i-1,j))/(8*dx)      
            endif

            ! ----- Delta ------- !
            ! delta_T(i,j) = sqrt((strain_11**2 + strain_22**2)*(1 + ellip_rat**(-2))  &
            !             + 4*(ellip_rat**(-2))*strain_12**2              &
            !             + 2*strain_11*strain_22*(1 - ellip_rat**(-2)))

            delta = sqrt((strain_11**2 + strain_22**2)*(1 + ellip_rat**(-2))  &
                        + 4*(ellip_rat**(-2))*strain_12**2              &
                        + 2*strain_11*strain_22*(1 - ellip_rat**(-2)))

            ! --- Max bulk visc coef ---- !
            zeta_max = maxvisc_cf*P_T(i,j)

            ! --- Calc bulk visc according to the capping --- !
            ! zeta_T(i,j) = min(P_T(i,j)/(2*delta + machine_eps*P_T(i,j)/zeta_max), zeta_max)
            ! zeta_T(i,j) = min(P_T(i,j)/(2*delta_T(i,j) + machine_eps*P_T(i,j)/zeta_max), zeta_max)

            ! --- Calc bulk visc according to the smoothed capping --- !
            zeta_T(i,j) = zeta_max*tanh(P_T(i,j)/(2*delta*zeta_max + machine_eps))

            ! --- Calc shear visc --- !
            eta_T(i,j) = zeta_T(i,j)*(ellip_rat**(-2))
        enddo            

        ! ------- Calculate Viscosities at N points ----------- !
        do ij = 1, nN_pnts
            i = indxNi(ij)
            j = indxNj(ij)

            ! ------ Strain Rates -------- !
            ! NOTE: at some point, it may be worth while to store these values in a matrix at each timestep
            ! NOTE: strain_11(du/dx) and strain_22(dv/dy) have to be interpolated at the node points

            if ((product(Nimsk(i,j,:)) == 0) .or. (product(Nlmsk(i,j,:)) == 0)) then
                ! masking is required !

                !===========================!
                ! mask velocities as needed !
                !===========================!
                    ! inner ring !
                    ! u_p0p0Tst = Nlmsk(i,j,2 )*(Nimsk(i,j,2 )*ugrid(i  ,j  ) + (1 - Nimsk(i,j,2 ))*ugrid(i  ,j-1))
                    ! u_p0m1Tst = Nlmsk(i,j,5 )*(Nimsk(i,j,5 )*ugrid(i  ,j-1) + (1 - Nimsk(i,j,5 ))*ugrid(i  ,j  ))
                    ! v_p0p0Tst = Nlmsk(i,j,11)*(Nimsk(i,j,11)*vgrid(i  ,j  ) + (1 - Nimsk(i,j,11))*vgrid(i-1,j  ))
                    ! v_m1p0Tst = Nlmsk(i,j,8 )*(Nimsk(i,j,8 )*vgrid(i-1,j  ) + (1 - Nimsk(i,j,8 ))*vgrid(i  ,j  ))

                    ! ! outer ring !
                    ! u_p1p0Tst = Nlmsk(i,j,3 )*(Nimsk(i,j,3 )*ugrid(i+1,j  ) + Nimsk(i,j,6 )*(1 - Nimsk(i,j,3 ))*ugrid(i+1,j-1) + (1 - Nimsk(i,j,6 ))*(1 - Nimsk(i,j,3 ))*u_p0p0Tst)
                    ! u_m1p0Tst = Nlmsk(i,j,1 )*(Nimsk(i,j,1 )*ugrid(i-1,j  ) + Nimsk(i,j,4 )*(1 - Nimsk(i,j,1 ))*ugrid(i-1,j-1) + (1 - Nimsk(i,j,4 ))*(1 - Nimsk(i,j,1 ))*u_p0p0Tst)
                    ! u_p1m1Tst = Nlmsk(i,j,6 )*(Nimsk(i,j,6 )*ugrid(i+1,j-1) + Nimsk(i,j,3 )*(1 - Nimsk(i,j,6 ))*ugrid(i+1,j  ) + (1 - Nimsk(i,j,3 ))*(1 - Nimsk(i,j,6 ))*u_p0m1Tst)
                    ! u_m1m1Tst = Nlmsk(i,j,4 )*(Nimsk(i,j,4 )*ugrid(i-1,j-1) + Nimsk(i,j,1 )*(1 - Nimsk(i,j,4 ))*ugrid(i-1,j  ) + (1 - Nimsk(i,j,1 ))*(1 - Nimsk(i,j,4 ))*u_p0m1Tst)
                    ! v_m1p1Tst = Nlmsk(i,j,7 )*(Nimsk(i,j,7 )*vgrid(i-1,j+1) + Nimsk(i,j,10)*(1 - Nimsk(i,j,7 ))*vgrid(i  ,j+1) + (1 - Nimsk(i,j,10))*(1 - Nimsk(i,j,7 ))*v_m1p0Tst)
                    ! v_m1m1Tst = Nlmsk(i,j,9 )*(Nimsk(i,j,9 )*vgrid(i-1,j-1) + Nimsk(i,j,12)*(1 - Nimsk(i,j,9 ))*vgrid(i  ,j-1) + (1 - Nimsk(i,j,12))*(1 - Nimsk(i,j,9 ))*v_m1p0Tst)
                    ! v_p0p1Tst = Nlmsk(i,j,10)*(Nimsk(i,j,10)*vgrid(i  ,j+1) + Nimsk(i,j,7 )*(1 - Nimsk(i,j,10))*vgrid(i-1,j+1) + (1 - Nimsk(i,j,7 ))*(1 - Nimsk(i,j,10))*v_p0p0Tst)
                    ! v_p0m1Tst = Nlmsk(i,j,12)*(Nimsk(i,j,12)*vgrid(i  ,j-1) + Nimsk(i,j,9 )*(1 - Nimsk(i,j,12))*vgrid(i-1,j-1) + (1 - Nimsk(i,j,9 ))*(1 - Nimsk(i,j,12))*v_p0p0Tst)

                    ! inner ring !
                    u_p0p0 = Nlmsk(i,j,2 )*(Nimsk(i,j,2 )*ugrid(i  ,j  ) + (1 - Nimsk(i,j,2 ))*(ugrid(i  ,j-1) + dx*dudyBC(x_N(i), y_N(j), time))) + (1 - Nlmsk(i,j,2 ))*uBC(x_u(i  ), y_u(j  ), time)
                    u_p0m1 = Nlmsk(i,j,5 )*(Nimsk(i,j,5 )*ugrid(i  ,j-1) + (1 - Nimsk(i,j,5 ))*(ugrid(i  ,j  ) - dx*dudyBC(x_N(i), y_N(j), time))) + (1 - Nlmsk(i,j,5 ))*uBC(x_u(i  ), y_u(j-1), time)
                    v_p0p0 = Nlmsk(i,j,11)*(Nimsk(i,j,11)*vgrid(i  ,j  ) + (1 - Nimsk(i,j,11))*(vgrid(i-1,j  ) + dx*dvdxBC(x_N(i), y_N(j), time))) + (1 - Nlmsk(i,j,11))*vBC(x_v(i  ), y_v(j  ), time)
                    v_m1p0 = Nlmsk(i,j,8 )*(Nimsk(i,j,8 )*vgrid(i-1,j  ) + (1 - Nimsk(i,j,8 ))*(vgrid(i  ,j  ) - dx*dvdxBC(x_N(i), y_N(j), time))) + (1 - Nlmsk(i,j,8 ))*vBC(x_v(i-1), y_v(j  ), time)

                    ! outer ring !
                    ! u(i+1,j) !
                    u_p1p0 = Nlmsk(i,j,3 )*(Nimsk(i,j,3 )*               ugrid(i+1,j  ) &       !******* NOTE:************ when using node points to define x and y coords of BC's +/- dx was used as there are no ghost node points. 
                            + Nimsk(i,j,6 )*(1 - Nimsk(i,j,3 ))*        (ugrid(i+1,j-1) + dx*dudyBC(x_N(i) + dx, y_N(j), time)) &   ! This fix avoids x_N and y_N going outside there bounds.
                            + (1 - Nimsk(i,j,6 ))*(1 - Nimsk(i,j,3 ))*  (u_p0p0         + dx*dudxBC(x_T(i  ), y_T(j  ), time))) &
                            + (1 - Nlmsk(i,j,3 ))*uBC(x_u(i+1), y_u(j), time)

                    ! u(i-1,j) !
                    u_m1p0 = Nlmsk(i,j,1 )*(Nimsk(i,j,1 )*               ugrid(i-1,j  ) & 
                            + Nimsk(i,j,4 )*(1 - Nimsk(i,j,1 ))*        (ugrid(i-1,j-1) + dx*dudyBC(x_N(i) - dx, y_N(j), time)) & 
                            + (1 - Nimsk(i,j,4 ))*(1 - Nimsk(i,j,1 ))*  (u_p0p0         - dx*dudxBC(x_T(i-1), y_T(j  ), time))) &
                            + (1 - Nlmsk(i,j,1 ))*uBC(x_u(i-1), y_u(j), time)

                    ! u(i+1,j-1) !
                    u_p1m1 = Nlmsk(i,j,6 )*(Nimsk(i,j,6 )*               ugrid(i+1,j-1) & 
                            + Nimsk(i,j,3 )*(1 - Nimsk(i,j,6 ))*        (ugrid(i+1,j  ) - dx*dudyBC(x_N(i) + dx, y_N(j), time)) & 
                            + (1 - Nimsk(i,j,3 ))*(1 - Nimsk(i,j,6 ))*  (u_p0m1         + dx*dudxBC(x_T(i  ), y_T(j-1), time))) &
                            + (1 - Nlmsk(i,j,6 ))*uBC(x_u(i+1), y_u(j-1), time)

                    ! u(i-1,j-1) !
                    u_m1m1 = Nlmsk(i,j,4 )*(Nimsk(i,j,4 )*               ugrid(i-1,j-1) & 
                            + Nimsk(i,j,1 )*(1 - Nimsk(i,j,4 ))*        (ugrid(i-1,j  ) - dx*dudyBC(x_N(i) - dx, y_N(j), time)) & 
                            + (1 - Nimsk(i,j,1 ))*(1 - Nimsk(i,j,4 ))*  (u_p0m1         - dx*dudxBC(x_T(i-1), y_T(j-1), time))) &
                            + (1 - Nlmsk(i,j,4 ))*uBC(x_u(i-1), y_u(j-1), time)

                    ! v(i-1,j+1) !
                    v_m1p1 = Nlmsk(i,j,7 )*(Nimsk(i,j,7 )*               vgrid(i-1,j+1) & 
                            + Nimsk(i,j,10)*(1 - Nimsk(i,j,7 ))*        (vgrid(i  ,j+1) - dx*dvdxBC(x_N(i), y_N(j) + dx, time)) & 
                            + (1 - Nimsk(i,j,10))*(1 - Nimsk(i,j,7 ))*  (v_m1p0         + dx*dvdyBC(x_T(i-1), y_T(j  ), time))) &
                            + (1 - Nlmsk(i,j,7 ))*vBC(x_v(i-1), y_v(j+1), time)

                    ! v(i-1,j-1) !
                    v_m1m1 = Nlmsk(i,j,9 )*(Nimsk(i,j,9 )*               vgrid(i-1,j-1) & 
                            + Nimsk(i,j,12)*(1 - Nimsk(i,j,9 ))*        (vgrid(i  ,j-1) - dx*dvdxBC(x_N(i), y_N(j) - dx, time)) & 
                            + (1 - Nimsk(i,j,12))*(1 - Nimsk(i,j,9 ))*  (v_m1p0         - dx*dvdyBC(x_T(i-1), y_T(j-1), time))) &
                            + (1 - Nlmsk(i,j,9 ))*vBC(x_v(i-1), y_v(j-1), time)

                    ! v(i,j+1) !
                    v_p0p1 = Nlmsk(i,j,10)*(Nimsk(i,j,10)*               vgrid(i  ,j+1) & 
                            + Nimsk(i,j,7 )*(1 - Nimsk(i,j,10))*        (vgrid(i-1,j+1) + dx*dvdxBC(x_N(i), y_N(j) + dx, time)) & 
                            + (1 - Nimsk(i,j,7 ))*(1 - Nimsk(i,j,10))*  (v_p0p0         + dx*dvdyBC(x_T(i  ), y_T(j  ), time))) &
                            + (1 - Nlmsk(i,j,10))*vBC(x_v(i), y_v(j+1), time)

                    ! v(i,j-1) !
                    v_p0m1 = Nlmsk(i,j,12)*(Nimsk(i,j,12)*               vgrid(i  ,j-1) & 
                            + Nimsk(i,j,9 )*(1 - Nimsk(i,j,12))*        (vgrid(i-1,j-1) + dx*dvdxBC(x_N(i), y_N(j) - dx, time)) & 
                            + (1 - Nimsk(i,j,9 ))*(1 - Nimsk(i,j,12))*  (v_p0p0         - dx*dvdyBC(x_T(i  ), y_T(j-1), time))) &
                            + (1 - Nlmsk(i,j,12))*vBC(x_v(i), y_v(j-1), time)

                ! if (((u_p0p0Tst - u_p0p0) /= 0) .or. ((u_p0m1Tst - u_p0m1) /= 0) .or. ((v_p0p0Tst - v_p0p0) /= 0) .or. ((v_m1p0Tst - v_m1p0) /= 0) .or. ((u_p1p0Tst - u_p1p0) /= 0) .or. ((u_m1p0Tst - u_m1p0) /= 0) .or. &
                !     ((u_p1m1Tst - u_p1m1) /= 0) .or. ((u_m1m1Tst - u_m1m1) /= 0) .or. ((v_m1p1Tst - v_m1p1) /= 0) .or. ((v_m1m1Tst - v_m1m1) /= 0) .or. ((v_p0p1Tst - v_p0p1) /= 0) .or. ((v_p0m1Tst - v_p0m1) /= 0)) then 
                !     print *, "Masking error in N point viscosity calculations"
                ! end if 

                strain_12 = (u_p0p0 - u_p0m1 + v_p0p0 - v_m1p0)/(2*dx)                                  !((1/2)(du/dy + dv/dx))
                strain_11 = ((u_p1p0 - u_m1p0) + (u_p1m1 - u_m1m1))/(4*dx)                              !(du/dx)
                strain_22 = ((v_m1p1 - v_m1m1) + (v_p0p1 - v_p0m1))/(4*dx)                              !(dv/dy)

            else 
                ! masking isn't required !

                strain_12 = (ugrid(i,j) - ugrid(i,j-1) + vgrid(i,j) - vgrid(i-1,j))/(2*dx)              !((1/2)(du/dy + dv/dx))
                strain_11 = (ugrid(i+1,j  ) - ugrid(i-1,j  ) + ugrid(i+1,j-1) - ugrid(i-1,j-1))/(4*dx)  !(du/dx)       
                strain_22 = (vgrid(i-1,j+1) - vgrid(i-1,j-1) + vgrid(i  ,j+1) - vgrid(i  ,j-1))/(4*dx)  !(dv/dy)
            endif

            ! ----- Delta ------- !
            ! delta_N(i,j) = sqrt((strain_11**2 + strain_22**2)*(1 + ellip_rat**(-2))  &
            !             + 4*(ellip_rat**(-2))*strain_12**2              &
            !             + 2*strain_11*strain_22*(1 - ellip_rat**(-2)))

            delta = sqrt((strain_11**2 + strain_22**2)*(1 + ellip_rat**(-2))  &
                        + 4*(ellip_rat**(-2))*strain_12**2              &
                        + 2*strain_11*strain_22*(1 - ellip_rat**(-2)))

            ! --- Max bulk visc coef ---- !
            zeta_max = maxvisc_cf*P_N(i,j)
            
            ! --- Calc bulk visc according to the non-smoothed capping method  --- !
            ! zeta_N(i,j) = min(P_N(i,j)/(2*delta + machine_eps*P_N(i,j)/zeta_max), zeta_max)
            ! zeta_N(i,j) = min(P_N(i,j)/(2*delta_N(i,j) + machine_eps*P_N(i,j)/zeta_max), zeta_max)

            ! --- Calc bulk visc according to smooth capping method --- !
            zeta_N(i,j) = zeta_max*tanh(P_N(i,j)/(2*delta*zeta_max + machine_eps))

            ! --- Calc shear visc --- !
            eta_N(i,j) = zeta_N(i,j)*(ellip_rat**(-2))

        enddo

        ! open(unit = 1, file = "../Validation_Tests/20km_Res_toy_vel/delta_N.txt", status = 'unknown')
        ! call csv_write_dble_2d(1, delta_N) 
        ! close(1)
        ! open(unit = 1, file = "../Validation_Tests/20km_Res_toy_vel/delta_T.txt", status = 'unknown')
        ! call csv_write_dble_2d(1, delta_T) 
        ! close(1)
        
    end subroutine calc_visc

!===========================================================================================!
!                           Calc Water Drag Coef                                            !
!===========================================================================================!
    subroutine calc_waterdrag_coef(Cw_u, Cw_v, ugrid, vgrid, uocn_u, vocn_u, uocn_v, vocn_v, &
                                    time, ulmsk, uimsk, vlmsk, vimsk, &
                                    nU_pnts, nV_pnts, nxU, nyU, nxV, nyV, &
                                    indxUi, indxUj, indxVi, indxVj)
        !=======================================================================================
        ! This subroutine calculates the water drag coefficient C_w at both u and v points. In 
        ! calculating the velocity vector at both types of points, bilinear approxmations are 
        ! used to get a u and v velocity at both points.
        !
        ! Note: in doing the bilinear approximation, masking must be used in order to be 
        !       consistent with the BC's used in matrix multiplications and population of 
        !       the RHS vector
        !=======================================================================================

        !==================!
        ! ----- In --------!
        !==================!
        integer(kind = int_kind), intent(in) :: &
            nxU, nyU, nxV, nyV,                 &   ! index limit for U and V points
            nU_pnts, nV_pnts                        ! number of U and V points in the computational domain
        integer(kind = int_kind), dimension(nU_pnts), intent(in) :: &
            indxUi, indxUj                          ! locations of U points in the comp. domain
        integer(kind = int_kind), dimension(nV_pnts), intent(in) :: &
            indxVi, indxVj                          ! locations of V points in the comp. domain

        real(kind = dbl_kind), intent(in) :: &
            time
        real(kind = dbl_kind), dimension(1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U), intent(in) :: &
            ugrid                                   ! u velocities on the grid
        real(kind = dbl_kind), dimension(1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V), intent(in) :: &
            vgrid                                   ! v velocities on the grid
        real(kind = dbl_kind), dimension(nxU,nyU), intent(in) :: &
            uocn_u, vocn_u                          ! ocean velocites at U points
        real(kind = dbl_kind), dimension(nxV,nyV), intent(in) :: &
            uocn_v, vocn_v                          ! ocean velocites at V points
        integer(kind = int_kind), dimension (nxU, nyU, 20), intent(in) :: &
            ulmsk, uimsk                            ! computational masks for U points  
        integer(kind = int_kind), dimension (nxV, nyV, 20), intent(in) :: &
            vlmsk, vimsk                            ! computational masks for V points

        !==================!
        !----- In/out -----!
        !==================!
        real(kind = dbl_kind), dimension(nxU, nyU), intent(inout) :: &
            Cw_u                                    ! water drag coefficient at U points
        real(kind = dbl_kind), dimension(nxV, nyV), intent(inout) :: &
            Cw_v                                    ! water drag coefficient at V points

        !==================!
        !--- Local Vars ---!
        !==================!
        integer(kind = int_kind) :: &
            i, j, ij                                ! local indicies
        real(kind = dbl_kind) :: &
            u_comp, v_comp, &                       ! will hold the u and v components of the velocity difference between the ice and ocean current.
            v1, v2, v3, v4, &                       ! when masking is required, will hold the masked v components needed for calculations
            u1, u2, u3, u4, &                       ! when masking is required, will hold the masked u components needed for calculations
            halfdx 

        halfdx = dx/2

        !------ Calculate Cw at U points -------!
        do ij = 1, nU_pnts
            i = indxUi(ij)
            j = indxUj(ij)

            u_comp = ugrid(i,j) - uocn_u(i,j)

            if ((product(ulmsk(i,j,:)) == 0) .or. (product(uimsk(i,j,:)) == 0)) then
                ! Masking is required as at least of the masks is activated !
                ! mask velocities at the surrounding v points around the u point of interest

                ! v(i-1,j) !
                v1 = ulmsk(i,j,5)*(uimsk(i,j,5)*                                         vgrid(i-1,j  ) &
                        +   uimsk(i,j,6)*(1 - uimsk(i,j,5))*                            (vgrid(i  ,j  ) - dx*dvdxBC(x_v(i) - halfdx, y_v(j), time)) &
                        +   uimsk(i,j,7)*(1 - uimsk(i,j,6))*(1 - uimsk(i,j,5))*         (vgrid(i-1,j+1) - dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time)) &
                        +   (1 - uimsk(i,j,7))*(1 - uimsk(i,j,6))*(1 - uimsk(i,j,5))*   (vgrid(i  ,j+1) - dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time) - dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time))) &
                        +   (1 - ulmsk(i,j,5))*vBC(x_v(i-1), y_v(j), time)
    
                ! v(i,j) !
                v2 = ulmsk(i,j,6)*(uimsk(i,j,6)*                                         vgrid(i  ,j  ) &  
                        +   uimsk(i,j,5)*(1 - uimsk(i,j,6))*                            (vgrid(i-1,j  ) + dx*dvdxBC(x_v(i) - halfdx, y_v(j), time)) &  
                        +   uimsk(i,j,8)*(1 - uimsk(i,j,5))*(1 - uimsk(i,j,6))*         (vgrid(i  ,j+1) - dx*dvdyBC(x_v(i), y_v(j) + halfdx, time)) &
                        +   (1 - uimsk(i,j,8))*(1 - uimsk(i,j,5))*(1 - uimsk(i,j,6))*   (vgrid(i-1,j+1) + dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time) - dx*dvdyBC(x_v(i), y_v(j) + halfdx, time))) &
                        +   (1 - ulmsk(i,j,6))*vBC(x_v(i), y_v(j), time)

                ! v(i-1,j+1) !
                v3 = ulmsk(i,j,7)*(uimsk(i,j,7)*                                         vgrid(i-1,j+1) &  
                        +   uimsk(i,j,8)*(1 - uimsk(i,j,7))*                            (vgrid(i  ,j+1) - dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time)) &  
                        +   uimsk(i,j,5)*(1 - uimsk(i,j,8))*(1 - uimsk(i,j,7))*         (vgrid(i-1,j  ) + dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time)) & 
                        +   (1 - uimsk(i,j,5))*(1 - uimsk(i,j,8))*(1 - uimsk(i,j,7))*   (vgrid(i  ,j  ) - dx*dvdxBC(x_v(i) - halfdx, y_v(j), time) + dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time))) &
                        +   (1 - ulmsk(i,j,7))*vBC(x_v(i-1), y_v(j+1), time)

                ! v(i,j+1) !
                v4 = ulmsk(i,j,8)*(uimsk(i,j,8)*                                         vgrid(i  ,j+1) &
                        +   uimsk(i,j,7)*(1 - uimsk(i,j,8))*                            (vgrid(i-1,j+1) + dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time)) &  
                        +   uimsk(i,j,6)*(1 - uimsk(i,j,7))*(1 - uimsk(i,j,8))*         (vgrid(i  ,j  ) + dx*dvdyBC(x_v(i), y_v(j) + halfdx, time )) & 
                        +   (1 - uimsk(i,j,6))*(1 - uimsk(i,j,7))*(1 - uimsk(i,j,8))*   (vgrid(i-1,j  ) + dx*dvdxBC(x_v(i) - halfdx, y_v(j), time) + dx*dvdyBC(x_v(i), y_v(j) + halfdx, time))) &
                        +   (1 - ulmsk(i,j,8))*vBC(x_v(i), y_v(j+1), time) 

                v_comp = (v1 + v2 + v3 + v4)/4 - vocn_u(i,j)

            else
                ! Masking isn't required !
                v_comp = (vgrid(i,j) + vgrid(i-1,j) + vgrid(i-1,j+1) + vgrid(i,j+1))/4 - vocn_u(i,j)
            endif

            Cw_u(i,j) = rho_w*Cd_w*sqrt(u_comp**2 + v_comp**2)
        enddo

        !------ Calculate Cw at V points -------!
        do ij = 1, nV_pnts
            i = indxVi(ij)
            j = indxVj(ij)

            v_comp = vgrid(i,j) - vocn_v(i,j)

            if ((product(vlmsk(i,j,:)) == 0) .or. (product(vimsk(i,j,:)) == 0)) then
                ! Masking is required as at least of the masks is activated !
                ! mask velocities at the surrounding v points around the u point of interest
                
                ! u(i,j-1) !
                u1 = vlmsk(i,j,1)*(vimsk(i,j,1)*                                     ugrid(i  ,j-1)  &
                        + vimsk(i,j,3)*(1 - vimsk(i,j,1))*                          (ugrid(i  ,j  ) - dx*dudyBC(x_u(i), y_u(j) - halfdx, time)) &
                        + vimsk(i,j,2)*(1 - vimsk(i,j,3))*(1 - vimsk(i,j,1))*       (ugrid(i+1,j-1) - dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time)) & 
                        + (1 - vimsk(i,j,2))*(1 - vimsk(i,j,3))*(1 - vimsk(i,j,1))* (ugrid(i+1,j  ) - dx*dudxBC(x_u(i) + halfdx, y_u(j), time) - dx*dudyBC(x_u(i), y_u(j) - halfdx, time))) &
                        + (1 - vlmsk(i,j,1))*uBC(x_u(i), y_u(j-1), time) 
                
                ! u(i+1,j-1) !
                u2 = vlmsk(i,j,2)*(vimsk(i,j,2)*                                     ugrid(i+1,j-1)  &
                        + vimsk(i,j,4)*(1 - vimsk(i,j,2))*                          (ugrid(i+1,j  ) - dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time)) & 
                        + vimsk(i,j,1)*(1 - vimsk(i,j,4))*(1 - vimsk(i,j,2))*       (ugrid(i  ,j-1) + dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time)) &
                        + (1 - vimsk(i,j,1))*(1 - vimsk(i,j,4))*(1 - vimsk(i,j,2))* (ugrid(i  ,j  ) + dx*dudxBC(x_u(i) + halfdx, y_u(j), time) - dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time))) &
                        + (1 - vlmsk(i,j,2))*uBC(x_u(i+1), y_u(j-1), time)
                
                ! u(i,j) !
                u3 = vlmsk(i,j,3)*(vimsk(i,j,3)*                                     ugrid(i  ,j  ) & 
                        + vimsk(i,j,1)*(1 - vimsk(i,j,3))*                          (ugrid(i  ,j-1) + dx*dudyBC(x_u(i), y_u(j) - halfdx, time)) & 
                        + vimsk(i,j,4)*(1 - vimsk(i,j,1))*(1 - vimsk(i,j,3))*       (ugrid(i+1,j  ) - dx*dudxBC(x_u(i) + halfdx, y_u(j), time)) &  
                        + (1 - vimsk(i,j,4))*(1 - vimsk(i,j,1))*(1 - vimsk(i,j,3))* (ugrid(i+1,j-1) - dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time) + dx*dudyBC(x_u(i), y_u(j) - halfdx, time))) &
                        + (1 - vlmsk(i,j,3))*uBC(x_u(i), y_u(j), time)
                
                ! u(i+1,j) !
                u4 = vlmsk(i,j,4)*(vimsk(i,j,4)*                                     ugrid(i+1,j  ) & 
                        + vimsk(i,j,2)*(1 - vimsk(i,j,4))*                          (ugrid(i+1,j-1) + dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time)) & 
                        + vimsk(i,j,3)*(1 - vimsk(i,j,2))*(1 - vimsk(i,j,4))*       (ugrid(i  ,j  ) + dx*dudxBC(x_u(i) + halfdx, y_u(j), time)) & 
                        + (1 - vimsk(i,j,3))*(1 - vimsk(i,j,2))*(1 - vimsk(i,j,4))* (ugrid(i  ,j-1) + dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time) + dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time))) &
                        + (1 - vlmsk(i,j,4))*uBC(x_u(i+1), y_u(j), time)                            

                u_comp = (u1 + u2 + u3 + u4)/4 - uocn_v(i,j)

            else
                ! Masking isn't required !
                u_comp = (ugrid(i,j) + ugrid(i+1,j) + ugrid(i+1,j-1) + ugrid(i,j-1))/4 - uocn_v(i,j)
            endif

            Cw_v(i,j) = rho_w*Cd_w*sqrt(u_comp**2 + v_comp**2)
        enddo                                        
    end subroutine calc_waterdrag_coef

!===========================================================================================!
!                           Interpolate h to u and v points                                 !
!===========================================================================================!
    subroutine interpolate_h(h_u, h_v, h_T, nxU, nyU, nxV, nyV, nxT, nyT, &
                                nU_pnts, nV_pnts, indxUi, indxUj, indxVi, indxVj)
        !================================================================================
        ! This subroutine interpolates h_T to the u and v points in order to clean up the 
        ! code when populating b and the matrix vector product.
        !
        ! The interpolation is a linear interpolation between the tracer points.
        !================================================================================

        !==================!
        ! ----- In --------!
        !==================!
        integer(kind = int_kind), intent(in) :: &
            nxU, nyU,   &                   ! indices limit - U points
            nxV, nyV,   &                   ! indices limit - V points
            nxT, nyT,   &                   ! indices limit - T points
            nU_pnts,    &                   ! number of U points in the comp. domain
            nV_pnts                         ! number of V points in the comp. domain
        integer(kind = int_kind), dimension(nU_pnts), intent(in) :: &
            indxUi, indxUj                  ! locations of U points in the comp. domain
        integer(kind = int_kind), dimension(nV_pnts), intent(in) :: &
            indxVi, indxVj                  ! locations of V points in the comp. domain
        real(kind = dbl_kind), dimension(1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent(in) :: &
            h_T                             ! ice thickness at tracer points

        !==================!
        ! ----- In/Out ----!
        !==================!
        real(kind = dbl_kind), dimension(nxU, nyU), intent(inout) :: &
            h_u                             ! ice thickness at U points
        real(kind = dbl_kind), dimension(nxV, nyV), intent(inout) :: &
            h_v                             ! ice thickness at V points

        !==================!
        ! ----- Local  ----!
        !==================!
        integer(kind = int_kind) :: &
            i,j, ij

        ! ----- Interpolate to U points ----- !
        do ij = 1, nU_pnts
            i = indxUi(ij)
            j = indxUj(ij)

            h_u(i,j) = (h_T(i,j) + h_T(i-1,j))/2
        enddo

        ! ----- Interpolate to V points ----- !
        do ij = 1, nV_pnts
            i = indxVi(ij)
            j = indxVj(ij)

            h_v(i,j) = (h_T(i,j) + h_T(i,j-1))/2
        enddo
    end subroutine interpolate_h

!===========================================================================================!
!                     Calculate and Save Normalized Principal Stresses                      !
!===========================================================================================!
    subroutine CalcSave_Pstress(time, ugrid, vgrid, eta_u, zeta_u, eta_v, zeta_v, P_u, P_v, &
                                indxUi, indxUj, indxVi, indxVj, nU_pnts, nV_pnts, &
                                uimsk, ulmsk, vimsk, vlmsk, nxU, nyU, nxV, nyV)
        !===================================================================================
        ! This routine calculates and saves the normalized principal stresses, sigma1 and 
        ! sigma2. The stresses are normalized by dividing by the ice strength at each point.
        !
        ! This routine doesn't store the principal stresses as they have no use in the solver,
        ! they are only used to assess the convergence of the solver.
        !
        ! Note, this routine has the ability to use inhomogenous BC's (its masking was 
        ! copied from the calculations of the viscosities.
        !===================================================================================

        !==================!
        ! ----- In --------!
        !==================!
        integer(kind = int_kind), intent (in) :: &
            nxU, nyU, nxV, nyV,     &   ! index limits for the U and V grids 
            nU_pnts, nV_pnts            ! number of U and V points in the comp domain 
        integer(kind = int_kind), dimension (nU_pnts), intent (in) :: &
            indxUi, indxUj              ! index locations for U points in comp domain 
        integer(kind = int_kind), dimension (nV_pnts), intent (in) :: &
            indxVi, indxVj              ! index locations for V points in comp domain
        integer(kind = int_kind), dimension (nxU, nyU, 20), intent (in) :: &
            uimsk, ulmsk                ! computational mask for setting BCs
        integer(kind = int_kind), dimension (nxV, nyV, 20), intent (in) :: &
            vimsk, vlmsk                ! computational mask for setting BCs

        real(kind = dbl_kind), intent (in) :: &
            time                        ! current time level 
        real(kind = dbl_kind), dimension (nxU, nyU), intent (in) :: &
            P_u, eta_u, zeta_u          ! U grid ice strength and viscosities
        real(kind = dbl_kind), dimension (1-gc_U:nxU+gc_U, 1-gc_U:nyU+gc_U), intent (in) :: &
            ugrid                       ! u velocities 
        real(kind = dbl_kind), dimension (nxV, nyV), intent (in) :: &
            P_v, eta_v, zeta_v          ! V grid ice strength and Viscosities
        real(kind = dbl_kind), dimension (1-gc_V:nxV+gc_V, 1-gc_V:nyV+gc_V), intent (in) :: &
            vgrid                       ! v velocities

        !==================!
        !--- Local Vars ---!
        !==================!
        character (len = 8) :: &
            fmt1, fmt2, dt_s, dx_s, time_s  ! strings for ouputting files 
        integer (kind = int_kind) :: &
            i, j, ij 
        real (kind = dbl_kind) :: &
            u_p0m1, u_m1p0, u_p1p0, u_p0p1, u_p0p0, u_p1m1,     &   ! masked u velocities 
            v_p0m1, v_m1p0, v_p1p0, v_p0p1, v_p0p0, v_m1p1,     &   ! masked v velocities 
            dudx, dudy, dvdx, dvdy,                             &   ! needed velocities 
            s11, s22, s12,                                      &   ! components of the stress tensor 
            cnst1, cnst2, cnst3, cnst4,                         &   ! useful constants 
            halfdx
        real (kind = dbl_kind), dimension (nxU, nyU) :: &
            s1_u, s2_u                                              ! principal stresses at U points 
            ! dudx_u, dudy_u, dvdx_u, dvdy_u                          ! derivative arrays for testing 
        real (kind = dbl_kind), dimension (nxV, nyV) :: &
            s1_v, s2_v                                              ! principal stresses at V points 
            ! dudx_v, dudy_v, dvdx_v, dvdy_v                          ! derivative arrays for testing 

        !============!
        ! Initialize !
        !============!
            s1_u = 0.d0
            s2_u = 0.d0
            s1_v = 0.d0
            s2_v = 0.d0

            ! dudx_u = 0.d0; dudy_u = 0.d0; dvdx_u = 0.d0; dvdy_u = 0.d0
            ! dudx_v = 0.d0; dudy_v = 0.d0; dvdx_v = 0.d0; dvdy_v = 0.d0

            fmt1 = '(I4.4)'
            fmt2 = '(I8.8)'
            write (dt_s, fmt1) int(dt)
            write (dx_s, fmt1) int(dx/1000)
            write (time_s, fmt2) int(time) 

            halfdx  = dx/2
            cnst1   = 1./(2*dx)

        !==============================!
        ! Calculate Principal Stresses !
        !==============================!

            ! U-points !
            do ij = 1,nU_pnts
                i = indxUi(ij)
                j = indxUj(ij)

                ! Check for masking !
                if ((product(ulmsk(i,j,:)) == 0) .or. (product(uimsk(i,j,:)) == 0)) then 
                    !===========================!
                    ! mask velocities as needed !
                    !===========================!   

                        !=======================!
                        !  Direct U Neighbours  !
                        !=======================!
                            u_p0m1 = ulmsk(i,j,1)*(uimsk(i,j,1)*ugrid(i  ,j-1)  +   (1 - uimsk(i,j,1))*(ugrid(i  ,j  ) - dx*dudyBC(x_u(i), y_u(j) - halfdx, time))) + (1 - ulmsk(i,j,1))*uBC(x_u(i)  , y_u(j-1), time)
                            u_m1p0 = ulmsk(i,j,2)*(uimsk(i,j,2)*ugrid(i-1,j  )  +   (1 - uimsk(i,j,2))*(ugrid(i  ,j  ) - dx*dudxBC(x_u(i) - halfdx, y_u(j), time))) + (1 - ulmsk(i,j,2))*uBC(x_u(i-1), y_u(j  ), time)
                            u_p1p0 = ulmsk(i,j,3)*(uimsk(i,j,3)*ugrid(i+1,j  )  +   (1 - uimsk(i,j,3))*(ugrid(i  ,j  ) + dx*dudxBC(x_u(i) + halfdx, y_u(j), time))) + (1 - ulmsk(i,j,3))*uBC(x_u(i+1), y_u(j  ), time)
                            u_p0p1 = ulmsk(i,j,4)*(uimsk(i,j,4)*ugrid(i  ,j+1)  +   (1 - uimsk(i,j,4))*(ugrid(i  ,j  ) + dx*dudyBC(x_u(i), y_u(j) + halfdx, time))) + (1 - ulmsk(i,j,4))*uBC(x_u(i  ), y_u(j+1), time)

                        !=======================!
                        !  Direct V Neighbours  !
                        !=======================!

                            ! v(i-1,j) !
                            v_m1p0 = ulmsk(i,j,5)*(uimsk(i,j,5)*                                     vgrid(i-1,j  ) &
                                    +   uimsk(i,j,6)*(1 - uimsk(i,j,5))*                            (vgrid(i  ,j  ) - dx*dvdxBC(x_v(i) - halfdx, y_v(j), time)) &
                                    +   uimsk(i,j,7)*(1 - uimsk(i,j,6))*(1 - uimsk(i,j,5))*         (vgrid(i-1,j+1) - dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time)) &
                                    +   (1 - uimsk(i,j,7))*(1 - uimsk(i,j,6))*(1 - uimsk(i,j,5))*   (vgrid(i  ,j+1) - dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time) - dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time))) &
                                    +   (1 - ulmsk(i,j,5))*vBC(x_v(i-1), y_v(j), time)

                            ! v(i,j) !
                            v_p0p0 = ulmsk(i,j,6)*(uimsk(i,j,6)*                                     vgrid(i  ,j  ) &  
                                    +   uimsk(i,j,5)*(1 - uimsk(i,j,6))*                            (vgrid(i-1,j  ) + dx*dvdxBC(x_v(i) - halfdx, y_v(j), time)) &  
                                    +   uimsk(i,j,8)*(1 - uimsk(i,j,5))*(1 - uimsk(i,j,6))*         (vgrid(i  ,j+1) - dx*dvdyBC(x_v(i), y_v(j) + halfdx, time)) &
                                    +   (1 - uimsk(i,j,8))*(1 - uimsk(i,j,5))*(1 - uimsk(i,j,6))*   (vgrid(i-1,j+1) + dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time) - dx*dvdyBC(x_v(i), y_v(j) + halfdx, time))) &
                                    +   (1 - ulmsk(i,j,6))*vBC(x_v(i), y_v(j), time)

                            ! v(i-1,j+1) !
                            v_m1p1 = ulmsk(i,j,7)*(uimsk(i,j,7)*                                     vgrid(i-1,j+1) &  
                                    +   uimsk(i,j,8)*(1 - uimsk(i,j,7))*                            (vgrid(i  ,j+1) - dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time)) &  
                                    +   uimsk(i,j,5)*(1 - uimsk(i,j,8))*(1 - uimsk(i,j,7))*         (vgrid(i-1,j  ) + dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time)) & 
                                    +   (1 - uimsk(i,j,5))*(1 - uimsk(i,j,8))*(1 - uimsk(i,j,7))*   (vgrid(i  ,j  ) - dx*dvdxBC(x_v(i) - halfdx, y_v(j), time) + dx*dvdyBC(x_v(i-1), y_v(j) + halfdx, time))) &
                                    +   (1 - ulmsk(i,j,7))*vBC(x_v(i-1), y_v(j+1), time)

                            ! v(i,j+1) !
                            v_p0p1 = ulmsk(i,j,8)*(uimsk(i,j,8)*                                     vgrid(i  ,j+1) &
                                    +   uimsk(i,j,7)*(1 - uimsk(i,j,8))*                            (vgrid(i-1,j+1) + dx*dvdxBC(x_v(i) - halfdx, y_v(j+1), time)) &  
                                    +   uimsk(i,j,6)*(1 - uimsk(i,j,7))*(1 - uimsk(i,j,8))*         (vgrid(i  ,j  ) + dx*dvdyBC(x_v(i), y_v(j) + halfdx, time )) & 
                                    +   (1 - uimsk(i,j,6))*(1 - uimsk(i,j,7))*(1 - uimsk(i,j,8))*   (vgrid(i-1,j  ) + dx*dvdxBC(x_v(i) - halfdx, y_v(j), time) + dx*dvdyBC(x_v(i), y_v(j) + halfdx, time))) &
                                    +   (1 - ulmsk(i,j,8))*vBC(x_v(i), y_v(j+1), time)

                    !===========================!
                    !   Calculate Derivatives   !
                    !===========================!
                        dudx = cnst1*(u_p1p0 - u_m1p0)
                        dudy = cnst1*(u_p0p1 - u_p0m1)
                        dvdx = cnst1*(v_p0p1 - v_m1p1 + v_p0p0 - v_m1p0)
                        dvdy = cnst1*(v_p0p1 - v_p0p0 + v_m1p1 - v_m1p0)

                        ! dudx_u(i,j) = dudx
                        ! dudy_u(i,j) = dudy
                        ! dvdx_u(i,j) = dvdx
                        ! dvdy_u(i,j) = dvdy
                else
                    ! Masking isn't required !

                    !===========================!
                    !   Calculate Derivatives   !
                    !===========================!
                        dudx = cnst1*(ugrid(i+1,j) - ugrid(i-1,j))
                        dudy = cnst1*(ugrid(i,j+1) - ugrid(i,j-1))
                        dvdx = cnst1*(vgrid(i,j+1) - vgrid(i-1,j+1) + vgrid(i,j) - vgrid(i-1,j))
                        dvdy = cnst1*(vgrid(i,j+1) - vgrid(i,j) + vgrid(i-1,j+1) - vgrid(i-1,j))

                        ! dudx_u(i,j) = dudx
                        ! dudy_u(i,j) = dudy
                        ! dvdx_u(i,j) = dvdx
                        ! dvdy_u(i,j) = dvdy
                end if 

                !====================!
                ! Calculate Stresses !  
                !====================!
                    s11 = (zeta_u(i,j) + eta_u(i,j))*dudx + (zeta_u(i,j) - eta_u(i,j))*dvdy - P_u(i,j)/2
                    s22 = (zeta_u(i,j) - eta_u(i,j))*dudx + (zeta_u(i,j) + eta_u(i,j))*dvdy - P_u(i,j)/2
                    s12 = eta_u(i,j)*(dudy + dvdx)

                    cnst2 = (s11 + s22)/2
                    cnst3 = (s11 - s22)/2
                    cnst4 = sqrt(cnst3**2 + s12**2)

                    ! Normalized (by P) principal stresses !
                    s1_u(i,j) = (cnst2 + cnst4)/P_u(i,j)
                    s2_u(i,j) = (cnst2 - cnst4)/P_u(i,j)
            end do 

            ! V-points !
            do ij = 1,nV_pnts
                i = indxVi(ij)
                j = indxVj(ij)

                ! Check for Masking !
                if ((product(vlmsk(i,j,:)) == 0) .or. (product(vimsk(i,j,:)) == 0)) then

                    !===========================!
                    ! mask velocities as needed !
                    !===========================! 

                        !=======================!
                        !  Direct U Neighbours  !
                        !=======================!

                            ! u(i,j-1) !
                            u_p0m1 = vlmsk(i,j,1)*(vimsk(i,j,1)*                                 ugrid(i  ,j-1)  &
                                    + vimsk(i,j,3)*(1 - vimsk(i,j,1))*                          (ugrid(i  ,j  ) - dx*dudyBC(x_u(i), y_u(j) - halfdx, time)) &
                                    + vimsk(i,j,2)*(1 - vimsk(i,j,3))*(1 - vimsk(i,j,1))*       (ugrid(i+1,j-1) - dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time)) & 
                                    + (1 - vimsk(i,j,2))*(1 - vimsk(i,j,3))*(1 - vimsk(i,j,1))* (ugrid(i+1,j  ) - dx*dudxBC(x_u(i) + halfdx, y_u(j), time) - dx*dudyBC(x_u(i), y_u(j) - halfdx, time))) &
                                    + (1 - vlmsk(i,j,1))*uBC(x_u(i), y_u(j-1), time) 
                            
                            ! u(i+1,j-1) !
                            u_p1m1 = vlmsk(i,j,2)*(vimsk(i,j,2)*                                 ugrid(i+1,j-1)  &
                                    + vimsk(i,j,4)*(1 - vimsk(i,j,2))*                          (ugrid(i+1,j  ) - dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time)) & 
                                    + vimsk(i,j,1)*(1 - vimsk(i,j,4))*(1 - vimsk(i,j,2))*       (ugrid(i  ,j-1) + dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time)) &
                                    + (1 - vimsk(i,j,1))*(1 - vimsk(i,j,4))*(1 - vimsk(i,j,2))* (ugrid(i  ,j  ) + dx*dudxBC(x_u(i) + halfdx, y_u(j), time) - dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time))) &
                                    + (1 - vlmsk(i,j,2))*uBC(x_u(i+1), y_u(j-1), time)
                            
                            ! u(i,j) !
                            u_p0p0 = vlmsk(i,j,3)*(vimsk(i,j,3)*                                 ugrid(i  ,j  ) & 
                                    + vimsk(i,j,1)*(1 - vimsk(i,j,3))*                          (ugrid(i  ,j-1) + dx*dudyBC(x_u(i), y_u(j) - halfdx, time)) & 
                                    + vimsk(i,j,4)*(1 - vimsk(i,j,1))*(1 - vimsk(i,j,3))*       (ugrid(i+1,j  ) - dx*dudxBC(x_u(i) + halfdx, y_u(j), time)) &  
                                    + (1 - vimsk(i,j,4))*(1 - vimsk(i,j,1))*(1 - vimsk(i,j,3))* (ugrid(i+1,j-1) - dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time) + dx*dudyBC(x_u(i), y_u(j) - halfdx, time))) &
                                    + (1 - vlmsk(i,j,3))*uBC(x_u(i), y_u(j), time)
                            
                            ! u(i+1,j) !
                            u_p1p0 = vlmsk(i,j,4)*(vimsk(i,j,4)*                                 ugrid(i+1,j  ) & 
                                    + vimsk(i,j,2)*(1 - vimsk(i,j,4))*                          (ugrid(i+1,j-1) + dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time)) & 
                                    + vimsk(i,j,3)*(1 - vimsk(i,j,2))*(1 - vimsk(i,j,4))*       (ugrid(i  ,j  ) + dx*dudxBC(x_u(i) + halfdx, y_u(j), time)) & 
                                    + (1 - vimsk(i,j,3))*(1 - vimsk(i,j,2))*(1 - vimsk(i,j,4))* (ugrid(i  ,j-1) + dx*dudxBC(x_u(i) + halfdx, y_u(j-1), time) + dx*dudyBC(x_u(i+1), y_u(j) - halfdx, time))) &
                                    + (1 - vlmsk(i,j,4))*uBC(x_u(i+1), y_u(j), time) 

                        !=======================!
                        !  Direct V Neighbours  !
                        !=======================!
                            v_p0m1 = vlmsk(i,j,5)*(vimsk(i,j,5)*vgrid(i  ,j-1) + (1 - vimsk(i,j,5))*(vgrid(i,j) - dx*dvdyBC(x_v(i), y_v(j) - halfdx, time))) + (1 - vlmsk(i,j,5))*vBC(x_v(i  ), y_v(j-1), time)  
                            v_m1p0 = vlmsk(i,j,6)*(vimsk(i,j,6)*vgrid(i-1,j  ) + (1 - vimsk(i,j,6))*(vgrid(i,j) - dx*dvdxBC(x_v(i) - halfdx, y_v(j), time))) + (1 - vlmsk(i,j,6))*vBC(x_v(i-1), y_v(j  ), time)
                            v_p1p0 = vlmsk(i,j,7)*(vimsk(i,j,7)*vgrid(i+1,j  ) + (1 - vimsk(i,j,7))*(vgrid(i,j) + dx*dvdxBC(x_v(i) + halfdx, y_v(j), time))) + (1 - vlmsk(i,j,7))*vBC(x_v(i+1), y_v(j  ), time)
                            v_p0p1 = vlmsk(i,j,8)*(vimsk(i,j,8)*vgrid(i  ,j+1) + (1 - vimsk(i,j,8))*(vgrid(i,j) + dx*dvdyBC(x_v(i), y_v(j) + halfdx, time))) + (1 - vlmsk(i,j,8))*vBC(x_v(i  ), y_v(j+1), time) 

                    !===========================!
                    !   Calculate Derivatives   !
                    !===========================!
                        dudx = cnst1*(u_p1p0 - u_p0p0 + u_p1m1 - u_p0m1)
                        dudy = cnst1*(u_p1p0 - u_p1m1 + u_p0p0 - u_p0m1)
                        dvdx = cnst1*(v_p1p0 - v_m1p0)
                        dvdy = cnst1*(v_p0p1 - v_p0m1)

                        ! dudx_v(i,j) = dudx
                        ! dudy_v(i,j) = dudy
                        ! dvdx_v(i,j) = dvdx
                        ! dvdy_v(i,j) = dvdy
                else
                    ! Masking isn't required !

                    !===========================!
                    !   Calculate Derivatives   !
                    !===========================!
                        dudx = cnst1*(ugrid(i+1,j) - ugrid(i,j) + ugrid(i+1,j-1) - ugrid(i,j-1))
                        dudy = cnst1*(ugrid(i+1,j) - ugrid(i+1,j-1) + ugrid(i,j) - ugrid(i,j-1))
                        dvdx = cnst1*(vgrid(i+1,j) - vgrid(i-1,j))
                        dvdy = cnst1*(vgrid(i,j+1) - vgrid(i,j-1))

                        ! dudx_v(i,j) = dudx
                        ! dudy_v(i,j) = dudy
                        ! dvdx_v(i,j) = dvdx
                        ! dvdy_v(i,j) = dvdy
                end if 

                !====================!
                ! Calculate Stresses !  
                !====================!
                    s11 = (zeta_v(i,j) + eta_v(i,j))*dudx + (zeta_v(i,j) - eta_v(i,j))*dvdy - P_v(i,j)/2
                    s22 = (zeta_v(i,j) - eta_v(i,j))*dudx + (zeta_v(i,j) + eta_v(i,j))*dvdy - P_v(i,j)/2
                    s12 = eta_v(i,j)*(dudy + dvdx)

                    cnst2 = (s11 + s22)/2
                    cnst3 = (s11 - s22)/2
                    cnst4 = sqrt(cnst3**2 + s12**2)

                    ! Normalized (by P) principal stresses !
                    s1_v(i,j) = (cnst2 + cnst4)/P_v(i,j)
                    s2_v(i,j) = (cnst2 - cnst4)/P_v(i,j)
            end do 

        !=========================!
        ! Save Principal Stresses !
        !=========================!
            open(unit = 1, file = './output/U_s1_dx'//trim(dx_s)//'dt'//trim(dt_s)//'time'//trim(time_s)//'.dat', status = 'unknown')
            open(unit = 2, file = './output/U_s2_dx'//trim(dx_s)//'dt'//trim(dt_s)//'time'//trim(time_s)//'.dat', status = 'unknown')
            open(unit = 3, file = './output/V_s1_dx'//trim(dx_s)//'dt'//trim(dt_s)//'time'//trim(time_s)//'.dat', status = 'unknown')
            open(unit = 4, file = './output/V_s2_dx'//trim(dx_s)//'dt'//trim(dt_s)//'time'//trim(time_s)//'.dat', status = 'unknown')
            call csv_write_dble_2d(1, s1_u)
            call csv_write_dble_2d(2, s2_u)
            call csv_write_dble_2d(3, s1_v)
            call csv_write_dble_2d(4, s2_v)
            close(1)
            close(2)
            close(3)
            close(4)

        !==============================!
        ! Save derivatives for testing !
        !==============================!
            ! open(unit = 1, file = './output/dudx_uStr.dat', status = 'unknown')
            ! open(unit = 2, file = './output/dudy_uStr.dat', status = 'unknown')
            ! open(unit = 3, file = './output/dvdx_uStr.dat', status = 'unknown')
            ! open(unit = 4, file = './output/dvdy_uStr.dat', status = 'unknown')
            ! call csv_write_dble_2d(1,dudx_u)
            ! call csv_write_dble_2d(2,dudy_u)
            ! call csv_write_dble_2d(3,dvdx_u)
            ! call csv_write_dble_2d(4,dvdy_u)
            ! close(1)
            ! close(2)
            ! close(3)
            ! close(4)

            ! open(unit = 1, file = './output/dudx_vStr.dat', status = 'unknown')
            ! open(unit = 2, file = './output/dudy_vStr.dat', status = 'unknown')
            ! open(unit = 3, file = './output/dvdx_vStr.dat', status = 'unknown')
            ! open(unit = 4, file = './output/dvdy_vStr.dat', status = 'unknown')
            ! call csv_write_dble_2d(1,dudx_v)
            ! call csv_write_dble_2d(2,dudy_v)
            ! call csv_write_dble_2d(3,dvdx_v)
            ! call csv_write_dble_2d(4,dvdy_v)
            ! close(1)
            ! close(2)
            ! close(3)
            ! close(4)
            

    end subroutine CalcSave_Pstress

!===========================================================================================!
!                           Calculate Volume                                                !
!===========================================================================================!
    real(kind = dbl_kind) function CalcVol(h_T, A_T, nxT, nyT)
        !====================================================================================
        ! This function calculates the volume of ice across the grid (not including ghost
        ! cells). 
        !====================================================================================

        !==========!
        !--- In ---!
        !==========!
        integer(kind = int_kind), intent (in) :: &
            nxT, nyT    ! index limits for T points 
        real(kind = dbl_kind), dimension (1-gc_T:nxT+gc_T, 1-gc_T:nyT+gc_T), intent (in) :: &
            h_T, A_T 

        !==================!
        !--- Local Vars ---!
        !==================!
        integer(kind = int_kind) :: &
            i, j        ! local indices 
        real(kind = dbl_kind) :: &
            Sum_Tmp,    &   ! temporary holder of volume as it is summed over the grid 
            Area            ! holds area of each cell 

        !============!
        ! Initialize !
        !============!
            Sum_Tmp = 0.d0
            Area    = dx*dx 

        !==================!
        ! Calculate Volume !
        !==================!
            do i = 1,nxT
                do j = 1,nyT
                    Sum_Tmp = Sum_Tmp + Area*h_T(i,j)*A_T(i,j)
                end do 
            end do

        !===============!
        ! Return Volume !
        !===============!
            CalcVol = Sum_Tmp

    end function CalcVol
end module var_routines
